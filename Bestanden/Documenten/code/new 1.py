Python 3.6.5 (v3.6.5:f59c0932b4, Mar 28 2018, 17:00:18) [MSC v.1900 64 bit (AMD64)] on win32
Type "copyright", "credits" or "license()" for more information.
>>> import tensorflow as tf
>>> sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
>>> import os, random, skimage
>>> from skimage import data
>>> from skimage import transform
>>> from skimage.color import rgb2gray
>>> import matplotlib.pyplot as plt
>>> import numpy as np
>>> def load_data(data_directory):
    directories = [d for d in os.listdir(data_directory)
                   if os.path.isdir(os.path.join(data_directory, d))]
    labels = []
    images = []
    for d in directories:
        label_directory = os.path.join(data_directory, d)
        file_names = [os.path.join(label_directory, f) 
                      for f in os.listdir(label_directory) 
                      if f.endswith(".jpg")]
        for f in file_names:
            images.append(skimage.data.imread(f))
            labels.append(int(d))
    return images, labels

>>> def show_some_images(images, filter):
    for i in range(len(filter)):
        plt.subplots_adjust(wspace=0.5)
        plt.subplot(1, 4, i+1)
        plt.axis('off')
        plt.imshow(images[filter[i]])
    plt.show()

    
>>> def show_some_gray_images(gray_images, filter):
    for i in range(len(filter)):
        plt.subplots_adjust(wspace=0.5)
        plt.subplot(1, 4, i+1)
        plt.axis('off')
        plt.imshow(gray_images[filter[i]], cmap="gray")
    plt.show()

>>> random.randrange(355)
232
>>> random.randrange(355)
22
>>> random.randrange(355)
271
>>> random.randrange(355)
170
>>> traffic_signs = [22, 170, 232, 271]
    
>>> ROOT_PATH = "D:\Bachelorproef\geknipt"
>>> train_data_directory = os.path.join(ROOT_PATH, "Training")
>>> test_data_directory = os.path.join(ROOT_PATH, "Testing")
>>> images, labels = load_data(train_data_directory)
>>> images_array = np.array(images)
>>> print(images_array.ndim)
1
>>> print(images_array.size)
533
>>> labels_array = np.array(labels)
>>> print(labels_array.ndim)
1
>>> print(labels_array.size)
533
>>> print(len(set(labels_array)))
3
	 
>>> images64 = [transform.resize(image, (64, 64)) for image in images]
	 
>>> images64_array = np.array(images64)
	 
>>> print(images64_array.shape)
	 
(533, 64, 64, 3)

>>> images64_gray = rgb2gray(images64_array)
	 
>>> print(images64_gray.shape)
	 
(533, 64, 64)

>>> show_some_images(images, traffic_signs)
>>> show_some_images(images64, traffic_signs)
>>> show_some_gray_images(images64_gray, traffic_signs)
	 
>>> x = tf.placeholder(dtype = tf.float32, shape = [None, 64, 64])
>>> y = tf.placeholder(dtype = tf.int32, shape = [None])
>>> images_flat = tf.contrib.layers.flatten(x)
>>> logits = tf.contrib.layers.fully_connected(images_flat, 3, tf.nn.relu)
>>> loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels = y, logits = logits))
>>> train_op = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)
>>> correct_pred = tf.argmax(logits, 1)
>>> accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
	 
>>> tf.set_random_seed(1234)
>>> sess = tf.Session()
>>> sess.run(tf.global_variables_initializer())
>>> for i in range(201):
	print('EPOCH', i)
	_, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images64_gray, y: labels})
	if i % 10 == 0:
		print("Loss: ", loss)
	print('DONE WITH EPOCH')

>>> sample_indexes = random.sample(range(len(images64_gray)), 10)
>>> sample_images = [images64_gray[i] for i in sample_indexes]
>>> sample_labels = [labels[i] for i in sample_indexes]
>>> predicted = sess.run([correct_pred], feed_dict={x: sample_images})[0]
>>> print(sample_labels)
[0, 0, 1, 2, 2, 2, 0, 2, 1, 2]
>>> print(predicted)
[0 0 0 0 0 0 0 0 0 0]

>>> fig = plt.figure(figsize=(10, 10))
	 
>>> for i in range(len(sample_images)):
    truth = sample_labels[i]
    prediction = predicted[i]
    plt.subplot(5, 2,1+i)
    plt.axis('off')
    color='green' if truth == prediction else 'red'
    plt.text(40, 10, "Truth:        {0}\nPrediction: {1}".format(truth, prediction), 
             fontsize=12, color=color)
    plt.imshow(sample_images[i], cmap="gray")
>>> plt.show()
	 
>>> x = tf.placeholder(dtype = tf.float32, shape = [None, 64, 64])
>>> y = tf.placeholder(dtype = tf.int32, shape = [None])
>>> images_flat = tf.contrib.layers.flatten(x)
>>> logits = tf.contrib.layers.fully_connected(images_flat, 3, tf.nn.leaky_relu)
>>> loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels = y, logits = logits))
>>> train_op = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)
>>> correct_pred = tf.argmax(logits, 1)
>>> accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
	 
>>> tf.set_random_seed(1234)
>>> sess = tf.Session()
>>> sess.run(tf.global_variables_initializer())
>>> for i in range(201):
	print('EPOCH', i)
	_, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images64_gray, y: labels})
	if i % 10 == 0:
		print("Loss: ", loss)
	print('DONE WITH EPOCH')

>>> sess.run([correct_pred], feed_dict={x: sample_images})[0]
	 
array([1, 0, 1, 2, 2, 1, 0, 2, 1, 1], dtype=int64)
>>> print(sample_labels)
	 
[0, 0, 1, 2, 2, 2, 0, 2, 1, 2]
>>> predicted = sess.run([correct_pred], feed_dict={x: sample_images})[0]
	 
>>> print(predicted)
	 
[1 0 1 2 2 1 0 2 1 1]
>>> test_images, test_labels = load_data(test_data_directory)
	 
>>> test_images64 = [transform.resize(image, (64, 64)) for image in test_images]

>>> test_images64 = rgb2gray(np.array(test_images64))
	 
>>> predicted = sess.run([correct_pred], feed_dict={x: test_images64})[0]
	 
>>> match_count = sum([int(y == y_) for y, y_ in zip(test_labels, predicted)])
	 
>>> accuracy = match_count / len(test_labels)
	 
>>> print("Accuracy: {:.3f}".format(accuracy))
	 
Accuracy: 0.576
>>> fig = plt.figure(figsize=(10, 10))
	 
>>> for i in range(len(sample_images)):
    truth = sample_labels[i]
    prediction = predicted[i]
    plt.subplot(5, 2,1+i)
    plt.axis('off')
    color='green' if truth == prediction else 'red'
    plt.text(40, 10, "Truth:        {0}\nPrediction: {1}".format(truth, prediction), 
             fontsize=12, color=color)
    plt.imshow(sample_images[i], cmap="gray")

>>> plt.show()
	 
