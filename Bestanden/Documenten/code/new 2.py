Python 3.6.5 (v3.6.5:f59c0932b4, Mar 28 2018, 17:00:18) [MSC v.1900 64 bit (AMD64)] on win32
Type "copyright", "credits" or "license()" for more information.
>>> import tensorflow as tf
>>> sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
>>> import os, random, skimage
>>> from skimage import data
>>> from skimage import transform
>>> from skimage.color import rgb2gray
>>> import matplotlib.pyplot as plt
>>> import numpy as np
>>> def load_data(data_directory):
    directories = [d for d in os.listdir(data_directory)
                   if os.path.isdir(os.path.join(data_directory, d))]
    labels = []
    images = []
    for d in directories:
        label_directory = os.path.join(data_directory, d)
        file_names = [os.path.join(label_directory, f) 
                      for f in os.listdir(label_directory) 
                      if f.endswith(".jpg")]
        for f in file_names:
            images.append(skimage.data.imread(f))
            labels.append(int(d))
    return images, labels

>>> def show_images(images):
    for i in range(len(images)):
        plt.subplots_adjust(wspace=0.5)
        plt.subplot(1, 4, i+1)
        plt.axis('off')
        plt.imshow(images[i])
    plt.show()

    
>>> def show_some_images(images, filter):
    for i in range(len(filter)):
        plt.subplots_adjust(wspace=0.5)
        plt.subplot(1, 4, i+1)
        plt.axis('off')
        plt.imshow(images[filter[i]])
    plt.show()

    
>>> def show_some_gray_images(images, filter):
    for i in range(len(filter)):
        plt.subplots_adjust(wspace=0.5)
        plt.subplot(1, 4, i+1)
        plt.axis('off')
        plt.imshow(images28_gray[filter[i]], cmap="gray")
    plt.show()

    
>>> ROOT_PATH = "D:\Bachelorproef\Processed"
>>> ROOT_PATH = "D:\Bachelorproef\geknipt"
>>> train_data_directory = os.path.join(ROOT_PATH, "Training")
>>> test_data_directory = os.path.join(ROOT_PATH, "Testing")
>>> images, labels = load_data(train_data_directory)
>>> 
>>> images_array = np.array(images)
>>> print(images_array.ndim)
1
>>> print(images_array.size)
533
>>> labels_array = np.array(labels)
>>> print(labels_array.ndim)
1
>>> print(labels_array.size)
533
>>> print(len(set(labels_array)))
3
>>> images6 = [transform.resize(image, (64, 64)) for image in images]

Warning (from warnings module):
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\skimage\transform\_warps.py", line 105
    warn("The default mode, 'constant', will be changed to 'reflect' in "
UserWarning: The default mode, 'constant', will be changed to 'reflect' in skimage 0.15.

Warning (from warnings module):
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\skimage\transform\_warps.py", line 110
    warn("Anti-aliasing will be enabled by default in skimage 0.15 to "
UserWarning: Anti-aliasing will be enabled by default in skimage 0.15 to avoid aliasing artifacts when down-sampling images.
>>> del images6
	 
>>> images64 = [transform.resize(image, (64, 64)) for image in images]
	 
>>> 
	 
>>> images64_array = np.array(images64)
	 
>>> print(images64_array.shape)
	 
(533, 64, 64, 3)
>>> images28_gray = rgb2gray(images28_array)
	 
Traceback (most recent call last):
  File "<pyshell#35>", line 1, in <module>
    images28_gray = rgb2gray(images28_array)
NameError: name 'images28_array' is not defined
>>> 
	 
>>> images64_gray = rgb2gray(images64_array)
	 
>>> print(images6_gray.shape)
	 
Traceback (most recent call last):
  File "<pyshell#38>", line 1, in <module>
    print(images6_gray.shape)
NameError: name 'images6_gray' is not defined
4
>>> 
	 
>>> print(images64_gray.shape)
	 
(533, 64, 64)
>>> x = tf.placeholder(dtype = tf.float32, shape = [None, 64, 64])
	 
>>> y = tf.placeholder(dtype = tf.int32, shape = [None])
	 
>>> images_flat = tf.contrib.layers.flatten(x)
	 
>>> print("images_flat: ", images_flat)
	 
images_flat:  Tensor("Flatten/flatten/Reshape:0", shape=(?, 4096), dtype=float32)
>>> logits = tf.contrib.layers.fully_connected(images_flat, 3, tf.nn.relu)
	 
>>> loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels = y, logits = logits))
	 
>>> train_op = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)
	 
>>> correct_pred = tf.argmax(logits, 1)
	 
>>> accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
	 
>>> tf.set_random_seed(1234)
	 
>>> sess = tf.Session()
	 
>>> sess.run(tf.global_variables_initializer())
	 
>>> for i in range(201):
	print('EPOCH', i)
	_, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images28_gray, y: labels})
	if i % 10 == 0:
		print("Loss: ", loss)
	print('DONE WITH EPOCH')

	 
EPOCH 0
Traceback (most recent call last):
  File "<pyshell#54>", line 3, in <module>
    _, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images28_gray, y: labels})
NameError: name 'images28_gray' is not defined
>>> for i in range(201):
	print('EPOCH', i)
	_, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images64_gray, y: labels})
	if i % 10 == 0:
		print("Loss: ", loss)
	print('DONE WITH EPOCH')

	 
EPOCH 0
Traceback (most recent call last):
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\tensorflow\python\client\session.py", line 1322, in _do_call
    return fn(*args)
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\tensorflow\python\client\session.py", line 1307, in _run_fn
    options, feed_dict, fetch_list, target_list, run_metadata)
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\tensorflow\python\client\session.py", line 1409, in _call_tf_sessionrun
    run_metadata)
tensorflow.python.framework.errors_impl.InternalError: Blas GEMM launch failed : a.shape=(533, 4096), b.shape=(4096, 3), m=533, n=3, k=4096
	 [[Node: fully_connected/MatMul = MatMul[T=DT_FLOAT, transpose_a=false, transpose_b=false, _device="/job:localhost/replica:0/task:0/device:GPU:0"](Flatten/flatten/Reshape, fully_connected/weights/read)]]

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "<pyshell#56>", line 3, in <module>
    _, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images64_gray, y: labels})
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\tensorflow\python\client\session.py", line 900, in run
    run_metadata_ptr)
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\tensorflow\python\client\session.py", line 1135, in _run
    feed_dict_tensor, options, run_metadata)
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\tensorflow\python\client\session.py", line 1316, in _do_run
    run_metadata)
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\tensorflow\python\client\session.py", line 1335, in _do_call
    raise type(e)(node_def, op, message)
tensorflow.python.framework.errors_impl.InternalError: Blas GEMM launch failed : a.shape=(533, 4096), b.shape=(4096, 3), m=533, n=3, k=4096
	 [[Node: fully_connected/MatMul = MatMul[T=DT_FLOAT, transpose_a=false, transpose_b=false, _device="/job:localhost/replica:0/task:0/device:GPU:0"](Flatten/flatten/Reshape, fully_connected/weights/read)]]

Caused by op 'fully_connected/MatMul', defined at:
  File "<string>", line 1, in <module>
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\idlelib\run.py", line 144, in main
    ret = method(*args, **kwargs)
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\idlelib\run.py", line 474, in runcode
    exec(code, self.locals)
  File "<pyshell#45>", line 1, in <module>
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\tensorflow\contrib\framework\python\ops\arg_scope.py", line 183, in func_with_args
    return func(*args, **current_args)
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\tensorflow\contrib\layers\python\layers\layers.py", line 1716, in fully_connected
    outputs = layer.apply(inputs)
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\tensorflow\python\layers\base.py", line 828, in apply
    return self.__call__(inputs, *args, **kwargs)
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\tensorflow\python\layers\base.py", line 717, in __call__
    outputs = self.call(inputs, *args, **kwargs)
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\tensorflow\python\layers\core.py", line 163, in call
    outputs = gen_math_ops.mat_mul(inputs, self.kernel)
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\tensorflow\python\ops\gen_math_ops.py", line 4567, in mat_mul
    name=name)
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\tensorflow\python\framework\op_def_library.py", line 787, in _apply_op_helper
    op_def=op_def)
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\tensorflow\python\framework\ops.py", line 3392, in create_op
    op_def=op_def)
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\tensorflow\python\framework\ops.py", line 1718, in __init__
    self._traceback = self._graph._extract_stack()  # pylint: disable=protected-access

InternalError (see above for traceback): Blas GEMM launch failed : a.shape=(533, 4096), b.shape=(4096, 3), m=533, n=3, k=4096
	 [[Node: fully_connected/MatMul = MatMul[T=DT_FLOAT, transpose_a=false, transpose_b=false, _device="/job:localhost/replica:0/task:0/device:GPU:0"](Flatten/flatten/Reshape, fully_connected/weights/read)]]

>>> nvidia-smi
Traceback (most recent call last):
  File "<pyshell#57>", line 1, in <module>
    nvidia-smi
NameError: name 'nvidia' is not defined
>>> for i in range(201):
	print('EPOCH', i)
	_, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images64_gray, y: labels})
	if i % 10 == 0:
		print("Loss: ", loss)
	print('DONE WITH EPOCH')

	
EPOCH 0

=============================== RESTART: Shell ===============================
>>> tf
Traceback (most recent call last):
  File "<pyshell#60>", line 1, in <module>
    tf
NameError: name 'tf' is not defined
>>> import tensorflow as tf
>>> sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
>>> import os, random, skimage
>>> from skimage import data
>>> from skimage import transform
>>> from skimage.color import rgb2gray
>>> import matplotlib.pyplot as plt
>>> import numpy as np
>>> def load_data(data_directory):
    directories = [d for d in os.listdir(data_directory)
                   if os.path.isdir(os.path.join(data_directory, d))]
    labels = []
    images = []
    for d in directories:
        label_directory = os.path.join(data_directory, d)
        file_names = [os.path.join(label_directory, f) 
                      for f in os.listdir(label_directory) 
                      if f.endswith(".jpg")]
        for f in file_names:
            images.append(skimage.data.imread(f))
            labels.append(int(d))
    return images, labels

>>> def show_images(images):
    for i in range(len(images)):
        plt.subplots_adjust(wspace=0.5)
        plt.subplot(1, 4, i+1)
        plt.axis('off')
        plt.imshow(images[i])
    plt.show()

    
>>> def show_some_images(images, filter):
    for i in range(len(filter)):
        plt.subplots_adjust(wspace=0.5)
        plt.subplot(1, 4, i+1)
        plt.axis('off')
        plt.imshow(images[filter[i]])
    plt.show()

    
>>> def show_some_gray_images(images, filter):
    for i in range(len(filter)):
        plt.subplots_adjust(wspace=0.5)
        plt.subplot(1, 4, i+1)
        plt.axis('off')
        plt.imshow(images28_gray[filter[i]], cmap="gray")
    plt.show()

    
>>> ROOT_PATH = "D:\Bachelorproef\geknipt"
>>> train_data_directory = os.path.join(ROOT_PATH, "Training")
>>> test_data_directory = os.path.join(ROOT_PATH, "Testing")
>>> images, labels = load_data(train_data_directory)
>>> images_array = np.array(images)
>>> print(images_array.ndim)
1
>>> print(images_array.size)
533
>>> labels_array = np.array(labels)
>>> print(labels_array.ndim)
1
>>> print(labels_array.size)
533
>>> print(len(set(labels_array)))
3
>>> images6 = [transform.resize(image, (64, 64)) for image in images]

Warning (from warnings module):
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\skimage\transform\_warps.py", line 105
    warn("The default mode, 'constant', will be changed to 'reflect' in "
UserWarning: The default mode, 'constant', will be changed to 'reflect' in skimage 0.15.

Warning (from warnings module):
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\skimage\transform\_warps.py", line 110
    warn("Anti-aliasing will be enabled by default in skimage 0.15 to "
UserWarning: Anti-aliasing will be enabled by default in skimage 0.15 to avoid aliasing artifacts when down-sampling images.
>>> del images6
	 
>>> images64 = [transform.resize(image, (64, 64)) for image in images]
	 
>>> images64_array = np.array(images64)
	 
>>> print(images64_array.shape)
	 
(533, 64, 64, 3)
>>> images28_gray = rgb2gray(images28_array)
	 
Traceback (most recent call last):
  File "<pyshell#93>", line 1, in <module>
    images28_gray = rgb2gray(images28_array)
NameError: name 'images28_array' is not defined
>>> images64_gray = rgb2gray(images64_array)
	 
>>> print(images64_gray.shape)
	 
(533, 64, 64)
>>> x = tf.placeholder(dtype = tf.float32, shape = [None, 64, 64])
	 
>>> x = tf.placeholder(dtype = tf.float32, shape = [None, 64, 64])
	 
>>> images_flat = tf.contrib.layers.flatten(x)
	 
>>> logits = tf.contrib.layers.fully_connected(images_flat, 3, tf.nn.relu)
	 
>>> loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels = y, logits = logits))
	 
Traceback (most recent call last):
  File "<pyshell#100>", line 1, in <module>
    loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels = y, logits = logits))
NameError: name 'y' is not defined
>>> y = tf.placeholder(dtype = tf.int32, shape = [None])
	 
>>> loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels = y, logits = logits))
	 
>>> train_op = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)
	 
>>> correct_pred = tf.argmax(logits, 1)
	 
>>> accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
	 
>>> tf.set_random_seed(1234)
	 
>>> sess = tf.Session()
	 
>>> sess.run(tf.global_variables_initializer())
	 
>>> for i in range(201):
	print('EPOCH', i)
	_, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images64_gray, y: labels})
	if i % 10 == 0:
		print("Loss: ", loss)
	print('DONE WITH EPOCH')

	 
EPOCH 0
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 1
DONE WITH EPOCH
EPOCH 2
DONE WITH EPOCH
EPOCH 3
DONE WITH EPOCH
EPOCH 4
DONE WITH EPOCH
EPOCH 5
DONE WITH EPOCH
EPOCH 6
DONE WITH EPOCH
EPOCH 7
DONE WITH EPOCH
EPOCH 8
DONE WITH EPOCH
EPOCH 9
DONE WITH EPOCH
EPOCH 10
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 11
DONE WITH EPOCH
EPOCH 12
DONE WITH EPOCH
EPOCH 13
DONE WITH EPOCH
EPOCH 14
DONE WITH EPOCH
EPOCH 15
DONE WITH EPOCH
EPOCH 16
DONE WITH EPOCH
EPOCH 17
DONE WITH EPOCH
EPOCH 18
DONE WITH EPOCH
EPOCH 19
DONE WITH EPOCH
EPOCH 20
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 21
DONE WITH EPOCH
EPOCH 22
DONE WITH EPOCH
EPOCH 23
DONE WITH EPOCH
EPOCH 24
DONE WITH EPOCH
EPOCH 25
DONE WITH EPOCH
EPOCH 26
DONE WITH EPOCH
EPOCH 27
DONE WITH EPOCH
EPOCH 28
DONE WITH EPOCH
EPOCH 29
DONE WITH EPOCH
EPOCH 30
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 31
DONE WITH EPOCH
EPOCH 32
DONE WITH EPOCH
EPOCH 33
DONE WITH EPOCH
EPOCH 34
DONE WITH EPOCH
EPOCH 35
DONE WITH EPOCH
EPOCH 36
DONE WITH EPOCH
EPOCH 37
DONE WITH EPOCH
EPOCH 38
DONE WITH EPOCH
EPOCH 39
DONE WITH EPOCH
EPOCH 40
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 41
DONE WITH EPOCH
EPOCH 42
DONE WITH EPOCH
EPOCH 43
DONE WITH EPOCH
EPOCH 44
DONE WITH EPOCH
EPOCH 45
DONE WITH EPOCH
EPOCH 46
DONE WITH EPOCH
EPOCH 47
DONE WITH EPOCH
EPOCH 48
DONE WITH EPOCH
EPOCH 49
DONE WITH EPOCH
EPOCH 50
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 51
DONE WITH EPOCH
EPOCH 52
DONE WITH EPOCH
EPOCH 53
DONE WITH EPOCH
EPOCH 54
DONE WITH EPOCH
EPOCH 55
DONE WITH EPOCH
EPOCH 56
DONE WITH EPOCH
EPOCH 57
DONE WITH EPOCH
EPOCH 58
DONE WITH EPOCH
EPOCH 59
DONE WITH EPOCH
EPOCH 60
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 61
DONE WITH EPOCH
EPOCH 62
DONE WITH EPOCH
EPOCH 63
DONE WITH EPOCH
EPOCH 64
DONE WITH EPOCH
EPOCH 65
DONE WITH EPOCH
EPOCH 66
DONE WITH EPOCH
EPOCH 67
DONE WITH EPOCH
EPOCH 68
DONE WITH EPOCH
EPOCH 69
DONE WITH EPOCH
EPOCH 70
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 71
DONE WITH EPOCH
EPOCH 72
DONE WITH EPOCH
EPOCH 73
DONE WITH EPOCH
EPOCH 74
DONE WITH EPOCH
EPOCH 75
DONE WITH EPOCH
EPOCH 76
DONE WITH EPOCH
EPOCH 77
DONE WITH EPOCH
EPOCH 78
DONE WITH EPOCH
EPOCH 79
DONE WITH EPOCH
EPOCH 80
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 81
DONE WITH EPOCH
EPOCH 82
DONE WITH EPOCH
EPOCH 83
DONE WITH EPOCH
EPOCH 84
DONE WITH EPOCH
EPOCH 85
DONE WITH EPOCH
EPOCH 86
DONE WITH EPOCH
EPOCH 87
DONE WITH EPOCH
EPOCH 88
DONE WITH EPOCH
EPOCH 89
DONE WITH EPOCH
EPOCH 90
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 91
DONE WITH EPOCH
EPOCH 92
DONE WITH EPOCH
EPOCH 93
DONE WITH EPOCH
EPOCH 94
DONE WITH EPOCH
EPOCH 95
DONE WITH EPOCH
EPOCH 96
DONE WITH EPOCH
EPOCH 97
DONE WITH EPOCH
EPOCH 98
DONE WITH EPOCH
EPOCH 99
DONE WITH EPOCH
EPOCH 100
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 101
DONE WITH EPOCH
EPOCH 102
DONE WITH EPOCH
EPOCH 103
DONE WITH EPOCH
EPOCH 104
DONE WITH EPOCH
EPOCH 105
DONE WITH EPOCH
EPOCH 106
DONE WITH EPOCH
EPOCH 107
DONE WITH EPOCH
EPOCH 108
DONE WITH EPOCH
EPOCH 109
DONE WITH EPOCH
EPOCH 110
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 111
DONE WITH EPOCH
EPOCH 112
DONE WITH EPOCH
EPOCH 113
DONE WITH EPOCH
EPOCH 114
DONE WITH EPOCH
EPOCH 115
DONE WITH EPOCH
EPOCH 116
DONE WITH EPOCH
EPOCH 117
DONE WITH EPOCH
EPOCH 118
DONE WITH EPOCH
EPOCH 119
DONE WITH EPOCH
EPOCH 120
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 121
DONE WITH EPOCH
EPOCH 122
DONE WITH EPOCH
EPOCH 123
DONE WITH EPOCH
EPOCH 124
DONE WITH EPOCH
EPOCH 125
DONE WITH EPOCH
EPOCH 126
DONE WITH EPOCH
EPOCH 127
DONE WITH EPOCH
EPOCH 128
DONE WITH EPOCH
EPOCH 129
DONE WITH EPOCH
EPOCH 130
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 131
DONE WITH EPOCH
EPOCH 132
DONE WITH EPOCH
EPOCH 133
DONE WITH EPOCH
EPOCH 134
DONE WITH EPOCH
EPOCH 135
DONE WITH EPOCH
EPOCH 136
DONE WITH EPOCH
EPOCH 137
DONE WITH EPOCH
EPOCH 138
DONE WITH EPOCH
EPOCH 139
DONE WITH EPOCH
EPOCH 140
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 141
DONE WITH EPOCH
EPOCH 142
DONE WITH EPOCH
EPOCH 143
DONE WITH EPOCH
EPOCH 144
DONE WITH EPOCH
EPOCH 145
DONE WITH EPOCH
EPOCH 146
DONE WITH EPOCH
EPOCH 147
DONE WITH EPOCH
EPOCH 148
DONE WITH EPOCH
EPOCH 149
DONE WITH EPOCH
EPOCH 150
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 151
DONE WITH EPOCH
EPOCH 152
DONE WITH EPOCH
EPOCH 153
DONE WITH EPOCH
EPOCH 154
DONE WITH EPOCH
EPOCH 155
DONE WITH EPOCH
EPOCH 156
DONE WITH EPOCH
EPOCH 157
DONE WITH EPOCH
EPOCH 158
DONE WITH EPOCH
EPOCH 159
DONE WITH EPOCH
EPOCH 160
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 161
DONE WITH EPOCH
EPOCH 162
DONE WITH EPOCH
EPOCH 163
DONE WITH EPOCH
EPOCH 164
DONE WITH EPOCH
EPOCH 165
DONE WITH EPOCH
EPOCH 166
DONE WITH EPOCH
EPOCH 167
DONE WITH EPOCH
EPOCH 168
DONE WITH EPOCH
EPOCH 169
DONE WITH EPOCH
EPOCH 170
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 171
DONE WITH EPOCH
EPOCH 172
DONE WITH EPOCH
EPOCH 173
DONE WITH EPOCH
EPOCH 174
DONE WITH EPOCH
EPOCH 175
DONE WITH EPOCH
EPOCH 176
DONE WITH EPOCH
EPOCH 177
DONE WITH EPOCH
EPOCH 178
DONE WITH EPOCH
EPOCH 179
DONE WITH EPOCH
EPOCH 180
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 181
DONE WITH EPOCH
EPOCH 182
DONE WITH EPOCH
EPOCH 183
DONE WITH EPOCH
EPOCH 184
DONE WITH EPOCH
EPOCH 185
DONE WITH EPOCH
EPOCH 186
DONE WITH EPOCH
EPOCH 187
DONE WITH EPOCH
EPOCH 188
DONE WITH EPOCH
EPOCH 189
DONE WITH EPOCH
EPOCH 190
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 191
DONE WITH EPOCH
EPOCH 192
DONE WITH EPOCH
EPOCH 193
DONE WITH EPOCH
EPOCH 194
DONE WITH EPOCH
EPOCH 195
DONE WITH EPOCH
EPOCH 196
DONE WITH EPOCH
EPOCH 197
DONE WITH EPOCH
EPOCH 198
DONE WITH EPOCH
EPOCH 199
DONE WITH EPOCH
EPOCH 200
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
>>> sample_indexes = random.sample(range(len(images28_gray)), 10)
	 
Traceback (most recent call last):
  File "<pyshell#111>", line 1, in <module>
    sample_indexes = random.sample(range(len(images28_gray)), 10)
NameError: name 'images28_gray' is not defined
>>> sample_indexes = random.sample(range(len(images64_gray)), 10)
	 
>>> sample_images = [images64_gray[i] for i in sample_indexes]
	 
>>> sample_labels = [labels[i] for i in sample_indexes]
	 
>>> predicted = sess.run([correct_pred], feed_dict={x: sample_images})[0]
	 
>>> print(sample_labels)
	 
[0, 0, 0, 0, 2, 1, 2, 0, 0, 1]
>>> print(predicted)
	 
[0 0 0 0 0 0 0 0 0 0]
>>> for i in range(201):
	print('EPOCH', i)
	_, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images64_gray, y: labels})
	if i % 10 == 0:
		print("Loss: ", loss)
	print('DONE WITH EPOCH')

	 
EPOCH 0
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 1
DONE WITH EPOCH
EPOCH 2
DONE WITH EPOCH
EPOCH 3
DONE WITH EPOCH
EPOCH 4
DONE WITH EPOCH
EPOCH 5
DONE WITH EPOCH
EPOCH 6
DONE WITH EPOCH
EPOCH 7
DONE WITH EPOCH
EPOCH 8
DONE WITH EPOCH
EPOCH 9
DONE WITH EPOCH
EPOCH 10
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 11
DONE WITH EPOCH
EPOCH 12
DONE WITH EPOCH
EPOCH 13
DONE WITH EPOCH
EPOCH 14
DONE WITH EPOCH
EPOCH 15
DONE WITH EPOCH
EPOCH 16
DONE WITH EPOCH
EPOCH 17
DONE WITH EPOCH
EPOCH 18
DONE WITH EPOCH
EPOCH 19
DONE WITH EPOCH
EPOCH 20
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 21
DONE WITH EPOCH
EPOCH 22
DONE WITH EPOCH
EPOCH 23
DONE WITH EPOCH
EPOCH 24
DONE WITH EPOCH
EPOCH 25
DONE WITH EPOCH
EPOCH 26
DONE WITH EPOCH
EPOCH 27
DONE WITH EPOCH
EPOCH 28
DONE WITH EPOCH
EPOCH 29
DONE WITH EPOCH
EPOCH 30
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 31
DONE WITH EPOCH
EPOCH 32
DONE WITH EPOCH
EPOCH 33
DONE WITH EPOCH
EPOCH 34
DONE WITH EPOCH
EPOCH 35
DONE WITH EPOCH
EPOCH 36
DONE WITH EPOCH
EPOCH 37
DONE WITH EPOCH
EPOCH 38
DONE WITH EPOCH
EPOCH 39
DONE WITH EPOCH
EPOCH 40
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 41
DONE WITH EPOCH
EPOCH 42
DONE WITH EPOCH
EPOCH 43
DONE WITH EPOCH
EPOCH 44
DONE WITH EPOCH
EPOCH 45
DONE WITH EPOCH
EPOCH 46
DONE WITH EPOCH
EPOCH 47
DONE WITH EPOCH
EPOCH 48
DONE WITH EPOCH
EPOCH 49
DONE WITH EPOCH
EPOCH 50
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 51
DONE WITH EPOCH
EPOCH 52
DONE WITH EPOCH
EPOCH 53
DONE WITH EPOCH
EPOCH 54
DONE WITH EPOCH
EPOCH 55
DONE WITH EPOCH
EPOCH 56
DONE WITH EPOCH
EPOCH 57
DONE WITH EPOCH
EPOCH 58
DONE WITH EPOCH
EPOCH 59
DONE WITH EPOCH
EPOCH 60
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 61
DONE WITH EPOCH
EPOCH 62
DONE WITH EPOCH
EPOCH 63
DONE WITH EPOCH
EPOCH 64
DONE WITH EPOCH
EPOCH 65
DONE WITH EPOCH
EPOCH 66
DONE WITH EPOCH
EPOCH 67
DONE WITH EPOCH
EPOCH 68
DONE WITH EPOCH
EPOCH 69
DONE WITH EPOCH
EPOCH 70
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 71
DONE WITH EPOCH
EPOCH 72
DONE WITH EPOCH
EPOCH 73
DONE WITH EPOCH
EPOCH 74
DONE WITH EPOCH
EPOCH 75
DONE WITH EPOCH
EPOCH 76
DONE WITH EPOCH
EPOCH 77
DONE WITH EPOCH
EPOCH 78
DONE WITH EPOCH
EPOCH 79
DONE WITH EPOCH
EPOCH 80
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 81
DONE WITH EPOCH
EPOCH 82
DONE WITH EPOCH
EPOCH 83
DONE WITH EPOCH
EPOCH 84
DONE WITH EPOCH
EPOCH 85
DONE WITH EPOCH
EPOCH 86
DONE WITH EPOCH
EPOCH 87
DONE WITH EPOCH
EPOCH 88
DONE WITH EPOCH
EPOCH 89
DONE WITH EPOCH
EPOCH 90
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 91
DONE WITH EPOCH
EPOCH 92
DONE WITH EPOCH
EPOCH 93
DONE WITH EPOCH
EPOCH 94
DONE WITH EPOCH
EPOCH 95
DONE WITH EPOCH
EPOCH 96
DONE WITH EPOCH
EPOCH 97
DONE WITH EPOCH
EPOCH 98
DONE WITH EPOCH
EPOCH 99
DONE WITH EPOCH
EPOCH 100
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 101
DONE WITH EPOCH
EPOCH 102
DONE WITH EPOCH
EPOCH 103
DONE WITH EPOCH
EPOCH 104
DONE WITH EPOCH
EPOCH 105
DONE WITH EPOCH
EPOCH 106
DONE WITH EPOCH
EPOCH 107
DONE WITH EPOCH
EPOCH 108
DONE WITH EPOCH
EPOCH 109
DONE WITH EPOCH
EPOCH 110
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 111
DONE WITH EPOCH
EPOCH 112
DONE WITH EPOCH
EPOCH 113
DONE WITH EPOCH
EPOCH 114
DONE WITH EPOCH
EPOCH 115
DONE WITH EPOCH
EPOCH 116
DONE WITH EPOCH
EPOCH 117
DONE WITH EPOCH
EPOCH 118
DONE WITH EPOCH
EPOCH 119
DONE WITH EPOCH
EPOCH 120
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 121
DONE WITH EPOCH
EPOCH 122
DONE WITH EPOCH
EPOCH 123
DONE WITH EPOCH
EPOCH 124
DONE WITH EPOCH
EPOCH 125
DONE WITH EPOCH
EPOCH 126
DONE WITH EPOCH
EPOCH 127
DONE WITH EPOCH
EPOCH 128
DONE WITH EPOCH
EPOCH 129
DONE WITH EPOCH
EPOCH 130
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 131
DONE WITH EPOCH
EPOCH 132
DONE WITH EPOCH
EPOCH 133
DONE WITH EPOCH
EPOCH 134
DONE WITH EPOCH
EPOCH 135
DONE WITH EPOCH
EPOCH 136
DONE WITH EPOCH
EPOCH 137
DONE WITH EPOCH
EPOCH 138
DONE WITH EPOCH
EPOCH 139
DONE WITH EPOCH
EPOCH 140
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 141
DONE WITH EPOCH
EPOCH 142
DONE WITH EPOCH
EPOCH 143
DONE WITH EPOCH
EPOCH 144
DONE WITH EPOCH
EPOCH 145
DONE WITH EPOCH
EPOCH 146
DONE WITH EPOCH
EPOCH 147
DONE WITH EPOCH
EPOCH 148
DONE WITH EPOCH
EPOCH 149
DONE WITH EPOCH
EPOCH 150
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 151
DONE WITH EPOCH
EPOCH 152
DONE WITH EPOCH
EPOCH 153
DONE WITH EPOCH
EPOCH 154
DONE WITH EPOCH
EPOCH 155
DONE WITH EPOCH
EPOCH 156
DONE WITH EPOCH
EPOCH 157
DONE WITH EPOCH
EPOCH 158
DONE WITH EPOCH
EPOCH 159
DONE WITH EPOCH
EPOCH 160
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 161
DONE WITH EPOCH
EPOCH 162
DONE WITH EPOCH
EPOCH 163
DONE WITH EPOCH
EPOCH 164
DONE WITH EPOCH
EPOCH 165
DONE WITH EPOCH
EPOCH 166
DONE WITH EPOCH
EPOCH 167
DONE WITH EPOCH
EPOCH 168
DONE WITH EPOCH
EPOCH 169
DONE WITH EPOCH
EPOCH 170
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 171
DONE WITH EPOCH
EPOCH 172
DONE WITH EPOCH
EPOCH 173
DONE WITH EPOCH
EPOCH 174
DONE WITH EPOCH
EPOCH 175
DONE WITH EPOCH
EPOCH 176
DONE WITH EPOCH
EPOCH 177
DONE WITH EPOCH
EPOCH 178
DONE WITH EPOCH
EPOCH 179
DONE WITH EPOCH
EPOCH 180
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 181
DONE WITH EPOCH
EPOCH 182
DONE WITH EPOCH
EPOCH 183
DONE WITH EPOCH
EPOCH 184
DONE WITH EPOCH
EPOCH 185
DONE WITH EPOCH
EPOCH 186
DONE WITH EPOCH
EPOCH 187
DONE WITH EPOCH
EPOCH 188
DONE WITH EPOCH
EPOCH 189
DONE WITH EPOCH
EPOCH 190
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 191
DONE WITH EPOCH
EPOCH 192
DONE WITH EPOCH
EPOCH 193
DONE WITH EPOCH
EPOCH 194
DONE WITH EPOCH
EPOCH 195
DONE WITH EPOCH
EPOCH 196
DONE WITH EPOCH
EPOCH 197
DONE WITH EPOCH
EPOCH 198
DONE WITH EPOCH
EPOCH 199
DONE WITH EPOCH
EPOCH 200
Loss:  Tensor("Mean:0", shape=(), dtype=float32)
DONE WITH EPOCH
>>> predicted = sess.run([correct_pred], feed_dict={x: sample_images})[0]
	 
>>> print(predicted)
	 
[0 0 0 0 0 0 0 0 0 0]
>>> def show_some_images(images, filter):
    for i in range(len(filter)):
        plt.subplots_adjust(wspace=0.5)
        plt.subplot(1, 4, i+1)
        plt.axis('off')
        plt.imshow(images[filter[i]])
    plt.show()

	 
>>> def show_some_gray_images(images, filter):
    for i in range(len(filter)):
        plt.subplots_adjust(wspace=0.5)
        plt.subplot(1, 4, i+1)
        plt.axis('off')
        plt.imshow(images28_gray[filter[i]], cmap="gray")
    plt.show()

	 
>>> random
	 
<module 'random' from 'C:\\Users\\Jorim Tielemans\\AppData\\Local\\Programs\\Python\\Python36\\lib\\random.py'>
>>> random()
	 
Traceback (most recent call last):
  File "<pyshell#127>", line 1, in <module>
    random()
TypeError: 'module' object is not callable
>>> random(5)
	 
Traceback (most recent call last):
  File "<pyshell#128>", line 1, in <module>
    random(5)
TypeError: 'module' object is not callable
>>> randrange(355)
	 
Traceback (most recent call last):
  File "<pyshell#129>", line 1, in <module>
    randrange(355)
NameError: name 'randrange' is not defined
>>> random.randrange(355)
	 
232
>>> random.randrange(355)
	 
22
>>> random.randrange(355)
	 
271
>>> random.randrange(355)
	 
170
>>> traffic_signs = [22, 170, 232, 271]
	 
>>> random.randrange(355)
	 
14
>>> traffic_signs = [22, 170, 232, 271]
	 
>>> randrange(355)
	 
Traceback (most recent call last):
  File "<pyshell#137>", line 1, in <module>
    randrange(355)
NameError: name 'randrange' is not defined
>>> random.randrange(355)
	 
134
>>> random.randrange(355)
	 
55
>>> random.randrange(355)
	 
228
>>> random.randrange(355)
	 
245
>>> random.randrange(355)
	 
128
>>> random.randrange(355)
	 
148
>>> random.randrange(355)
	 
261
>>> random.randrange(355)
	 
206
>>> random.randrange(355)
	 
223
>>> show_some_images(images, traffic_signs)
	 
>>> show_images(images64, traffic_signs)
	 
Traceback (most recent call last):
  File "<pyshell#148>", line 1, in <module>
    show_images(images64, traffic_signs)
TypeError: show_images() takes 1 positional argument but 2 were given
>>> 
	 
>>> show_some_images(images64, traffic_signs)
	 
>>> show_some_gray_images(images64_gray, traffic_signs)
	 
Traceback (most recent call last):
  File "<pyshell#151>", line 1, in <module>
    show_some_gray_images(images64_gray, traffic_signs)
  File "<pyshell#125>", line 6, in show_some_gray_images
    plt.imshow(images28_gray[filter[i]], cmap="gray")
NameError: name 'images28_gray' is not defined
>>> show_some_gray_images(images64_gray, traffic_signs)
	 

Warning (from warnings module):
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\matplotlib\cbook\deprecation.py", line 107
    warnings.warn(message, mplDeprecation, stacklevel=1)
MatplotlibDeprecationWarning: Adding an axes using the same arguments as a previous axes currently reuses the earlier instance.  In a future version, a new instance will always be created and returned.  Meanwhile, this warning can be suppressed, and the future behavior ensured, by passing a unique label to each axes instance.
Traceback (most recent call last):
  File "<pyshell#152>", line 1, in <module>
    show_some_gray_images(images64_gray, traffic_signs)
  File "<pyshell#125>", line 6, in show_some_gray_images
    plt.imshow(images28_gray[filter[i]], cmap="gray")
NameError: name 'images28_gray' is not defined
>>> 
	 
>>> images64_gray
	 
array([[[0.65003107, 0.65140936, 0.65633843, ..., 0.62688549,
         0.63365632, 0.62520392],
        [0.64597255, 0.64205098, 0.65321467, ..., 0.61904235,
         0.61268297, 0.61637633],
        [0.64261647, 0.64691904, 0.64609183, ..., 0.6277127 ,
         0.62688549, 0.62745098],
        ...,
        [0.54792456, 0.5448302 , 0.55354431, ..., 0.59106181,
         0.59412902, 0.59020745],
        [0.55002754, 0.54539569, 0.56044036, ..., 0.59366598,
         0.59412902, 0.58628588],
        [0.54875176, 0.54204745, 0.5409174 , ..., 0.59148176,
         0.58825025, 0.58881569]],

       [[0.61649951, 0.61956469, 0.61995804, ..., 0.59468574,
         0.59456319, 0.5876698 ],
        [0.61628941, 0.62069243, 0.62137083, ..., 0.59053798,
         0.59159137, 0.59356353],
        [0.61713863, 0.6146    , 0.61786885, ..., 0.58932422,
         0.59635906, 0.59943451],
        ...,
        [0.52098274, 0.52297529, 0.52456964, ..., 0.55388746,
         0.55505577, 0.55348729],
        [0.52720411, 0.52098132, 0.52787144, ..., 0.55811479,
         0.55261448, 0.55024402],
        [0.52099383, 0.52184328, 0.51961922, ..., 0.55042627,
         0.55042627, 0.54664166]],

       [[0.63617498, 0.64066706, 0.63560054, ..., 0.61904235,
         0.61904235, 0.62101451],
        [0.63225843, 0.63477333, 0.63477333, ..., 0.60543188,
         0.61317137, 0.59836221],
        [0.63225843, 0.64076789, 0.63767353, ..., 0.62493608,
         0.62101451, 0.61373686],
        ...,
        [0.53670077, 0.54199652, 0.54880167, ..., 0.58059681,
         0.57750245, 0.56720951],
        [0.53616863, 0.53703789, 0.54258032, ..., 0.58724733,
         0.58308067, 0.58126314],
        [0.53486348, 0.53958338, 0.53673412, ..., 0.57929711,
         0.57452118, 0.57844275]],

       ...,

       [[0.52632218, 0.536766  , 0.55629725, ..., 0.67196556,
         0.67588113, 0.67014135],
        [0.53459424, 0.53624517, 0.56182728, ..., 0.66376206,
         0.66413395, 0.66790483],
        [0.53108627, 0.53658218, 0.55629725, ..., 0.66635765,
         0.67406292, 0.66856353],
        ...,
        [0.49971373, 0.4972614 , 0.49579216, ..., 0.58987468,
         0.59356353, 0.59412902],
        [0.49411765, 0.49364277, 0.49747373, ..., 0.59581059,
         0.59359333, 0.60094757],
        [0.49187059, 0.48920301, 0.49085608, ..., 0.59086745,
         0.58988706, 0.59497154]],

       [[0.58853381, 0.59542279, 0.62225137, ..., 0.74707804,
         0.75214627, 0.74764353],
        [0.58213176, 0.60181074, 0.62130032, ..., 0.74554353,
         0.74788863, 0.74673637],
        [0.58623218, 0.6008912 , 0.6193102 , ..., 0.74200627,
         0.73948   , 0.74465409],
        ...,
        [0.54843922, 0.55273309, 0.5487362 , ..., 0.65462275,
         0.65985456, 0.6600126 ],
        [0.55250078, 0.55224694, 0.5463712 , ..., 0.65471725,
         0.65398868, 0.65292498],
        [0.54037225, 0.53407662, 0.54012877, ..., 0.65951779,
         0.64394286, 0.6533539 ]],

       [[0.53109547, 0.54805156, 0.57341953, ..., 0.69711922,
         0.69677961, 0.69098282],
        [0.53746466, 0.5504439 , 0.57890956, ..., 0.69488147,
         0.69827735, 0.69952902],
        [0.52787725, 0.55446968, 0.5680698 , ..., 0.69033669,
         0.6843436 , 0.70157586],
        ...,
        [0.50558471, 0.49938763, 0.50392512, ..., 0.6189851 ,
         0.62373904, 0.62021098],
        [0.50556196, 0.50137998, 0.50730828, ..., 0.61541843,
         0.61284025, 0.61873917],
        [0.49953255, 0.49484105, 0.50351159, ..., 0.60545255,
         0.61555289, 0.61481882]]])
>>> def show_some_gray_images(images, filter):
    for i in range(len(filter)):
        plt.subplots_adjust(wspace=0.5)
        plt.subplot(1, 4, i+1)
        plt.axis('off')
        plt.imshow(images[filter[i]], cmap="gray")
    plt.show()

	 
>>> show_some_gray_images(images64_gray, traffic_signs)
	 
>>> set(labels)
	 
{0, 1, 2}
>>> x = tf.placeholder(dtype = tf.float32, shape = [None, 28, 28])
	 
>>> x = tf.placeholder(dtype = tf.float32, shape = [None, 64, 64])
	 
>>> y = tf.placeholder(dtype = tf.int32, shape = [None])
	 
>>> images_flat = tf.contrib.layers.flatten(x)
	 
>>> logits = tf.contrib.layers.fully_connected(images_flat, 3, tf.nn.relu)
	 
>>> loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels = y, logits = logits))
	 
>>> train_op = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)
	 
>>> correct_pred = tf.argmax(logits, 1)
	 
>>> accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
	 
>>> tf.set_random_seed(1234)
	 
>>> sess = tf.Session()
	 
>>> sess.run(tf.global_variables_initializer())
	 
>>> for i in range(201):
	print('EPOCH', i)
	_, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images28_gray, y: labels})
	if i % 10 == 0:
		print("Loss: ", loss)
	print('DONE WITH EPOCH')

	 
EPOCH 0
Traceback (most recent call last):
  File "<pyshell#172>", line 3, in <module>
    _, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images28_gray, y: labels})
NameError: name 'images28_gray' is not defined
>>> for i in range(201):
	print('EPOCH', i)
	_, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images_64gray, y: labels})
	if i % 10 == 0:
		print("Loss: ", loss)
	print('DONE WITH EPOCH')

	 
EPOCH 0
Traceback (most recent call last):
  File "<pyshell#174>", line 3, in <module>
    _, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images_64gray, y: labels})
NameError: name 'images_64gray' is not defined
>>> for i in range(201):
	print('EPOCH', i)
	_, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images64_gray, y: labels})
	if i % 10 == 0:
		print("Loss: ", loss)
	print('DONE WITH EPOCH')

	 
EPOCH 0
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 1
DONE WITH EPOCH
EPOCH 2
DONE WITH EPOCH
EPOCH 3
DONE WITH EPOCH
EPOCH 4
DONE WITH EPOCH
EPOCH 5
DONE WITH EPOCH
EPOCH 6
DONE WITH EPOCH
EPOCH 7
DONE WITH EPOCH
EPOCH 8
DONE WITH EPOCH
EPOCH 9
DONE WITH EPOCH
EPOCH 10
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 11
DONE WITH EPOCH
EPOCH 12
DONE WITH EPOCH
EPOCH 13
DONE WITH EPOCH
EPOCH 14
DONE WITH EPOCH
EPOCH 15
DONE WITH EPOCH
EPOCH 16
DONE WITH EPOCH
EPOCH 17
DONE WITH EPOCH
EPOCH 18
DONE WITH EPOCH
EPOCH 19
DONE WITH EPOCH
EPOCH 20
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 21
DONE WITH EPOCH
EPOCH 22
DONE WITH EPOCH
EPOCH 23
DONE WITH EPOCH
EPOCH 24
DONE WITH EPOCH
EPOCH 25
DONE WITH EPOCH
EPOCH 26
DONE WITH EPOCH
EPOCH 27
DONE WITH EPOCH
EPOCH 28
DONE WITH EPOCH
EPOCH 29
DONE WITH EPOCH
EPOCH 30
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 31
DONE WITH EPOCH
EPOCH 32
DONE WITH EPOCH
EPOCH 33
DONE WITH EPOCH
EPOCH 34
DONE WITH EPOCH
EPOCH 35
DONE WITH EPOCH
EPOCH 36
DONE WITH EPOCH
EPOCH 37
DONE WITH EPOCH
EPOCH 38
DONE WITH EPOCH
EPOCH 39
DONE WITH EPOCH
EPOCH 40
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 41
DONE WITH EPOCH
EPOCH 42
DONE WITH EPOCH
EPOCH 43
DONE WITH EPOCH
EPOCH 44
DONE WITH EPOCH
EPOCH 45
DONE WITH EPOCH
EPOCH 46
DONE WITH EPOCH
EPOCH 47
DONE WITH EPOCH
EPOCH 48
DONE WITH EPOCH
EPOCH 49
DONE WITH EPOCH
EPOCH 50
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 51
DONE WITH EPOCH
EPOCH 52
DONE WITH EPOCH
EPOCH 53
DONE WITH EPOCH
EPOCH 54
DONE WITH EPOCH
EPOCH 55
DONE WITH EPOCH
EPOCH 56
DONE WITH EPOCH
EPOCH 57
DONE WITH EPOCH
EPOCH 58
DONE WITH EPOCH
EPOCH 59
DONE WITH EPOCH
EPOCH 60
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 61
DONE WITH EPOCH
EPOCH 62
DONE WITH EPOCH
EPOCH 63
DONE WITH EPOCH
EPOCH 64
DONE WITH EPOCH
EPOCH 65
DONE WITH EPOCH
EPOCH 66
DONE WITH EPOCH
EPOCH 67
DONE WITH EPOCH
EPOCH 68
DONE WITH EPOCH
EPOCH 69
DONE WITH EPOCH
EPOCH 70
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 71
DONE WITH EPOCH
EPOCH 72
DONE WITH EPOCH
EPOCH 73
DONE WITH EPOCH
EPOCH 74
DONE WITH EPOCH
EPOCH 75
DONE WITH EPOCH
EPOCH 76
DONE WITH EPOCH
EPOCH 77
DONE WITH EPOCH
EPOCH 78
DONE WITH EPOCH
EPOCH 79
DONE WITH EPOCH
EPOCH 80
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 81
DONE WITH EPOCH
EPOCH 82
DONE WITH EPOCH
EPOCH 83
DONE WITH EPOCH
EPOCH 84
DONE WITH EPOCH
EPOCH 85
DONE WITH EPOCH
EPOCH 86
DONE WITH EPOCH
EPOCH 87
DONE WITH EPOCH
EPOCH 88
DONE WITH EPOCH
EPOCH 89
DONE WITH EPOCH
EPOCH 90
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 91
DONE WITH EPOCH
EPOCH 92
DONE WITH EPOCH
EPOCH 93
DONE WITH EPOCH
EPOCH 94
DONE WITH EPOCH
EPOCH 95
DONE WITH EPOCH
EPOCH 96
DONE WITH EPOCH
EPOCH 97
DONE WITH EPOCH
EPOCH 98
DONE WITH EPOCH
EPOCH 99
DONE WITH EPOCH
EPOCH 100
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 101
DONE WITH EPOCH
EPOCH 102
DONE WITH EPOCH
EPOCH 103
DONE WITH EPOCH
EPOCH 104
DONE WITH EPOCH
EPOCH 105
DONE WITH EPOCH
EPOCH 106
DONE WITH EPOCH
EPOCH 107
DONE WITH EPOCH
EPOCH 108
DONE WITH EPOCH
EPOCH 109
DONE WITH EPOCH
EPOCH 110
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 111
DONE WITH EPOCH
EPOCH 112
DONE WITH EPOCH
EPOCH 113
DONE WITH EPOCH
EPOCH 114
DONE WITH EPOCH
EPOCH 115
DONE WITH EPOCH
EPOCH 116
DONE WITH EPOCH
EPOCH 117
DONE WITH EPOCH
EPOCH 118
DONE WITH EPOCH
EPOCH 119
DONE WITH EPOCH
EPOCH 120
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 121
DONE WITH EPOCH
EPOCH 122
DONE WITH EPOCH
EPOCH 123
DONE WITH EPOCH
EPOCH 124
DONE WITH EPOCH
EPOCH 125
DONE WITH EPOCH
EPOCH 126
DONE WITH EPOCH
EPOCH 127
DONE WITH EPOCH
EPOCH 128
DONE WITH EPOCH
EPOCH 129
DONE WITH EPOCH
EPOCH 130
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 131
DONE WITH EPOCH
EPOCH 132
DONE WITH EPOCH
EPOCH 133
DONE WITH EPOCH
EPOCH 134
DONE WITH EPOCH
EPOCH 135
DONE WITH EPOCH
EPOCH 136
DONE WITH EPOCH
EPOCH 137
DONE WITH EPOCH
EPOCH 138
DONE WITH EPOCH
EPOCH 139
DONE WITH EPOCH
EPOCH 140
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 141
DONE WITH EPOCH
EPOCH 142
DONE WITH EPOCH
EPOCH 143
DONE WITH EPOCH
EPOCH 144
DONE WITH EPOCH
EPOCH 145
DONE WITH EPOCH
EPOCH 146
DONE WITH EPOCH
EPOCH 147
DONE WITH EPOCH
EPOCH 148
DONE WITH EPOCH
EPOCH 149
DONE WITH EPOCH
EPOCH 150
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 151
DONE WITH EPOCH
EPOCH 152
DONE WITH EPOCH
EPOCH 153
DONE WITH EPOCH
EPOCH 154
DONE WITH EPOCH
EPOCH 155
DONE WITH EPOCH
EPOCH 156
DONE WITH EPOCH
EPOCH 157
DONE WITH EPOCH
EPOCH 158
DONE WITH EPOCH
EPOCH 159
DONE WITH EPOCH
EPOCH 160
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 161
DONE WITH EPOCH
EPOCH 162
DONE WITH EPOCH
EPOCH 163
DONE WITH EPOCH
EPOCH 164
DONE WITH EPOCH
EPOCH 165
DONE WITH EPOCH
EPOCH 166
DONE WITH EPOCH
EPOCH 167
DONE WITH EPOCH
EPOCH 168
DONE WITH EPOCH
EPOCH 169
DONE WITH EPOCH
EPOCH 170
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 171
DONE WITH EPOCH
EPOCH 172
DONE WITH EPOCH
EPOCH 173
DONE WITH EPOCH
EPOCH 174
DONE WITH EPOCH
EPOCH 175
DONE WITH EPOCH
EPOCH 176
DONE WITH EPOCH
EPOCH 177
DONE WITH EPOCH
EPOCH 178
DONE WITH EPOCH
EPOCH 179
DONE WITH EPOCH
EPOCH 180
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 181
DONE WITH EPOCH
EPOCH 182
DONE WITH EPOCH
EPOCH 183
DONE WITH EPOCH
EPOCH 184
DONE WITH EPOCH
EPOCH 185
DONE WITH EPOCH
EPOCH 186
DONE WITH EPOCH
EPOCH 187
DONE WITH EPOCH
EPOCH 188
DONE WITH EPOCH
EPOCH 189
DONE WITH EPOCH
EPOCH 190
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 191
DONE WITH EPOCH
EPOCH 192
DONE WITH EPOCH
EPOCH 193
DONE WITH EPOCH
EPOCH 194
DONE WITH EPOCH
EPOCH 195
DONE WITH EPOCH
EPOCH 196
DONE WITH EPOCH
EPOCH 197
DONE WITH EPOCH
EPOCH 198
DONE WITH EPOCH
EPOCH 199
DONE WITH EPOCH
EPOCH 200
Loss:  Tensor("Mean_2:0", shape=(), dtype=float32)
DONE WITH EPOCH
>>> sample_indexes = random.sample(range(len(images64_gray)), 10)
	 
>>> sample_images = [images64_gray[i] for i in sample_indexes]
	 
>>> sample_labels = [labels[i] for i in sample_indexes]
	 
>>> predicted = sess.run([correct_pred], feed_dict={x: sample_images})[0]
	 
>>> print(sample_labels)
	 
[0, 0, 1, 2, 2, 2, 0, 2, 1, 2]
>>> print(predicted)
	 
[0 0 0 0 0 0 0 0 0 0]
>>> fig = plt.figure(figsize=(10, 10))
	 
>>> for i in range(len(sample_images)):
    truth = sample_labels[i]
    prediction = predicted[i]
    plt.subplot(5, 2,1+i)
    plt.axis('off')
    color='green' if truth == prediction else 'red'
    plt.text(40, 10, "Truth:        {0}\nPrediction: {1}".format(truth, prediction), 
             fontsize=12, color=color)
    plt.imshow(sample_images[i], cmap="gray")

	 
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B1839080>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        0\nPrediction: 0')
<matplotlib.image.AxesImage object at 0x00000137B1E1CEB8>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B1E1CE48>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        0\nPrediction: 0')
<matplotlib.image.AxesImage object at 0x00000137B1E4EA90>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B1E4EDA0>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        1\nPrediction: 0')
<matplotlib.image.AxesImage object at 0x00000137B1E7E668>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B1E7E978>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        2\nPrediction: 0')
<matplotlib.image.AxesImage object at 0x00000137B2EA1278>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B2EA1588>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        2\nPrediction: 0')
<matplotlib.image.AxesImage object at 0x00000137B2EC6DD8>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B2EC6D68>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        2\nPrediction: 0')
<matplotlib.image.AxesImage object at 0x00000137B2EF89B0>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B2EF8CC0>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        0\nPrediction: 0')
<matplotlib.image.AxesImage object at 0x00000137B2F2C588>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B2F2C898>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        2\nPrediction: 0')
<matplotlib.image.AxesImage object at 0x00000137B2F5C160>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B2F53EB8>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        1\nPrediction: 0')
<matplotlib.image.AxesImage object at 0x00000137B2F83CF8>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B2F83C88>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        2\nPrediction: 0')
<matplotlib.image.AxesImage object at 0x00000137B2FB78D0>
>>> plt.show()
	 
>>> x = tf.placeholder(dtype = tf.float32, shape = [None, 64, 64])
	 
>>> y = tf.placeholder(dtype = tf.int32, shape = [None])
	 
>>> images_flat = tf.contrib.layers.flatten(x)
	 
>>> logits = tf.contrib.layers.fully_connected(images_flat, 3, tf.nn.leaky_relu)
	 
>>> loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels = y, logits = logits))
	 
>>> train_op = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)
	 
>>> correct_pred = tf.argmax(logits, 1)
	 
>>> accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
	 
>>> tf.set_random_seed(1234)
	 
>>> sess = tf.Session()
	 
>>> sess.run(tf.global_variables_initializer())
	 
>>> for i in range(201):
	print('EPOCH', i)
	_, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images64_gray, y: labels})
	if i % 10 == 0:
		print("Loss: ", loss)
	print('DONE WITH EPOCH')

	 
EPOCH 0
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 1
DONE WITH EPOCH
EPOCH 2
DONE WITH EPOCH
EPOCH 3
DONE WITH EPOCH
EPOCH 4
DONE WITH EPOCH
EPOCH 5
DONE WITH EPOCH
EPOCH 6
DONE WITH EPOCH
EPOCH 7
DONE WITH EPOCH
EPOCH 8
DONE WITH EPOCH
EPOCH 9
DONE WITH EPOCH
EPOCH 10
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 11
DONE WITH EPOCH
EPOCH 12
DONE WITH EPOCH
EPOCH 13
DONE WITH EPOCH
EPOCH 14
DONE WITH EPOCH
EPOCH 15
DONE WITH EPOCH
EPOCH 16
DONE WITH EPOCH
EPOCH 17
DONE WITH EPOCH
EPOCH 18
DONE WITH EPOCH
EPOCH 19
DONE WITH EPOCH
EPOCH 20
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 21
DONE WITH EPOCH
EPOCH 22
DONE WITH EPOCH
EPOCH 23
DONE WITH EPOCH
EPOCH 24
DONE WITH EPOCH
EPOCH 25
DONE WITH EPOCH
EPOCH 26
DONE WITH EPOCH
EPOCH 27
DONE WITH EPOCH
EPOCH 28
DONE WITH EPOCH
EPOCH 29
DONE WITH EPOCH
EPOCH 30
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 31
DONE WITH EPOCH
EPOCH 32
DONE WITH EPOCH
EPOCH 33
DONE WITH EPOCH
EPOCH 34
DONE WITH EPOCH
EPOCH 35
DONE WITH EPOCH
EPOCH 36
DONE WITH EPOCH
EPOCH 37
DONE WITH EPOCH
EPOCH 38
DONE WITH EPOCH
EPOCH 39
DONE WITH EPOCH
EPOCH 40
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 41
DONE WITH EPOCH
EPOCH 42
DONE WITH EPOCH
EPOCH 43
DONE WITH EPOCH
EPOCH 44
DONE WITH EPOCH
EPOCH 45
DONE WITH EPOCH
EPOCH 46
DONE WITH EPOCH
EPOCH 47
DONE WITH EPOCH
EPOCH 48
DONE WITH EPOCH
EPOCH 49
DONE WITH EPOCH
EPOCH 50
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 51
DONE WITH EPOCH
EPOCH 52
DONE WITH EPOCH
EPOCH 53
DONE WITH EPOCH
EPOCH 54
DONE WITH EPOCH
EPOCH 55
DONE WITH EPOCH
EPOCH 56
DONE WITH EPOCH
EPOCH 57
DONE WITH EPOCH
EPOCH 58
DONE WITH EPOCH
EPOCH 59
DONE WITH EPOCH
EPOCH 60
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 61
DONE WITH EPOCH
EPOCH 62
DONE WITH EPOCH
EPOCH 63
DONE WITH EPOCH
EPOCH 64
DONE WITH EPOCH
EPOCH 65
DONE WITH EPOCH
EPOCH 66
DONE WITH EPOCH
EPOCH 67
DONE WITH EPOCH
EPOCH 68
DONE WITH EPOCH
EPOCH 69
DONE WITH EPOCH
EPOCH 70
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 71
DONE WITH EPOCH
EPOCH 72
DONE WITH EPOCH
EPOCH 73
DONE WITH EPOCH
EPOCH 74
DONE WITH EPOCH
EPOCH 75
DONE WITH EPOCH
EPOCH 76
DONE WITH EPOCH
EPOCH 77
DONE WITH EPOCH
EPOCH 78
DONE WITH EPOCH
EPOCH 79
DONE WITH EPOCH
EPOCH 80
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 81
DONE WITH EPOCH
EPOCH 82
DONE WITH EPOCH
EPOCH 83
DONE WITH EPOCH
EPOCH 84
DONE WITH EPOCH
EPOCH 85
DONE WITH EPOCH
EPOCH 86
DONE WITH EPOCH
EPOCH 87
DONE WITH EPOCH
EPOCH 88
DONE WITH EPOCH
EPOCH 89
DONE WITH EPOCH
EPOCH 90
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 91
DONE WITH EPOCH
EPOCH 92
DONE WITH EPOCH
EPOCH 93
DONE WITH EPOCH
EPOCH 94
DONE WITH EPOCH
EPOCH 95
DONE WITH EPOCH
EPOCH 96
DONE WITH EPOCH
EPOCH 97
DONE WITH EPOCH
EPOCH 98
DONE WITH EPOCH
EPOCH 99
DONE WITH EPOCH
EPOCH 100
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 101
DONE WITH EPOCH
EPOCH 102
DONE WITH EPOCH
EPOCH 103
DONE WITH EPOCH
EPOCH 104
DONE WITH EPOCH
EPOCH 105
DONE WITH EPOCH
EPOCH 106
DONE WITH EPOCH
EPOCH 107
DONE WITH EPOCH
EPOCH 108
DONE WITH EPOCH
EPOCH 109
DONE WITH EPOCH
EPOCH 110
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 111
DONE WITH EPOCH
EPOCH 112
DONE WITH EPOCH
EPOCH 113
DONE WITH EPOCH
EPOCH 114
DONE WITH EPOCH
EPOCH 115
DONE WITH EPOCH
EPOCH 116
DONE WITH EPOCH
EPOCH 117
DONE WITH EPOCH
EPOCH 118
DONE WITH EPOCH
EPOCH 119
DONE WITH EPOCH
EPOCH 120
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 121
DONE WITH EPOCH
EPOCH 122
DONE WITH EPOCH
EPOCH 123
DONE WITH EPOCH
EPOCH 124
DONE WITH EPOCH
EPOCH 125
DONE WITH EPOCH
EPOCH 126
DONE WITH EPOCH
EPOCH 127
DONE WITH EPOCH
EPOCH 128
DONE WITH EPOCH
EPOCH 129
DONE WITH EPOCH
EPOCH 130
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 131
DONE WITH EPOCH
EPOCH 132
DONE WITH EPOCH
EPOCH 133
DONE WITH EPOCH
EPOCH 134
DONE WITH EPOCH
EPOCH 135
DONE WITH EPOCH
EPOCH 136
DONE WITH EPOCH
EPOCH 137
DONE WITH EPOCH
EPOCH 138
DONE WITH EPOCH
EPOCH 139
DONE WITH EPOCH
EPOCH 140
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 141
DONE WITH EPOCH
EPOCH 142
DONE WITH EPOCH
EPOCH 143
DONE WITH EPOCH
EPOCH 144
DONE WITH EPOCH
EPOCH 145
DONE WITH EPOCH
EPOCH 146
DONE WITH EPOCH
EPOCH 147
DONE WITH EPOCH
EPOCH 148
DONE WITH EPOCH
EPOCH 149
DONE WITH EPOCH
EPOCH 150
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 151
DONE WITH EPOCH
EPOCH 152
DONE WITH EPOCH
EPOCH 153
DONE WITH EPOCH
EPOCH 154
DONE WITH EPOCH
EPOCH 155
DONE WITH EPOCH
EPOCH 156
DONE WITH EPOCH
EPOCH 157
DONE WITH EPOCH
EPOCH 158
DONE WITH EPOCH
EPOCH 159
DONE WITH EPOCH
EPOCH 160
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 161
DONE WITH EPOCH
EPOCH 162
DONE WITH EPOCH
EPOCH 163
DONE WITH EPOCH
EPOCH 164
DONE WITH EPOCH
EPOCH 165
DONE WITH EPOCH
EPOCH 166
DONE WITH EPOCH
EPOCH 167
DONE WITH EPOCH
EPOCH 168
DONE WITH EPOCH
EPOCH 169
DONE WITH EPOCH
EPOCH 170
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 171
DONE WITH EPOCH
EPOCH 172
DONE WITH EPOCH
EPOCH 173
DONE WITH EPOCH
EPOCH 174
DONE WITH EPOCH
EPOCH 175
DONE WITH EPOCH
EPOCH 176
DONE WITH EPOCH
EPOCH 177
DONE WITH EPOCH
EPOCH 178
DONE WITH EPOCH
EPOCH 179
DONE WITH EPOCH
EPOCH 180
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 181
DONE WITH EPOCH
EPOCH 182
DONE WITH EPOCH
EPOCH 183
DONE WITH EPOCH
EPOCH 184
DONE WITH EPOCH
EPOCH 185
DONE WITH EPOCH
EPOCH 186
DONE WITH EPOCH
EPOCH 187
DONE WITH EPOCH
EPOCH 188
DONE WITH EPOCH
EPOCH 189
DONE WITH EPOCH
EPOCH 190
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
EPOCH 191
DONE WITH EPOCH
EPOCH 192
DONE WITH EPOCH
EPOCH 193
DONE WITH EPOCH
EPOCH 194
DONE WITH EPOCH
EPOCH 195
DONE WITH EPOCH
EPOCH 196
DONE WITH EPOCH
EPOCH 197
DONE WITH EPOCH
EPOCH 198
DONE WITH EPOCH
EPOCH 199
DONE WITH EPOCH
EPOCH 200
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH
>>> sess.run([correct_pred], feed_dict={x: sample_images})[0]
	 
array([1, 0, 1, 2, 2, 1, 0, 2, 1, 1], dtype=int64)
>>> print(sample_labels)
	 
[0, 0, 1, 2, 2, 2, 0, 2, 1, 2]
>>> predicted = sess.run([correct_pred], feed_dict={x: sample_images})[0]
	 
>>> print(predicted)
	 
[1 0 1 2 2 1 0 2 1 1]
>>> print("logits: ", logits)
	 
logits:  Tensor("fully_connected_2/LeakyRelu/Maximum:0", shape=(?, 3), dtype=float32)
>>> test_images, test_labels = load_data(test_data_directory)
	 
>>> test_images64 = [transform.resize(image, (64, 64)) for image in test_images]
	 

Warning (from warnings module):
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\skimage\transform\_warps.py", line 105
    warn("The default mode, 'constant', will be changed to 'reflect' in "
UserWarning: The default mode, 'constant', will be changed to 'reflect' in skimage 0.15.

Warning (from warnings module):
  File "C:\Users\Jorim Tielemans\AppData\Local\Programs\Python\Python36\lib\site-packages\skimage\transform\_warps.py", line 110
    warn("Anti-aliasing will be enabled by default in skimage 0.15 to "
UserWarning: Anti-aliasing will be enabled by default in skimage 0.15 to avoid aliasing artifacts when down-sampling images.
>>> test_images64 = rgb2gray(np.array(test_images64))
	 
>>> predicted = sess.run([correct_pred], feed_dict={x: test_images64})[0]
	 
>>> match_count = sum([int(y == y_) for y, y_ in zip(test_labels, predicted)])
	 
>>> accuracy = match_count / len(test_labels)
	 
>>> print("Accuracy: {:.3f}".format(accuracy))
	 
Accuracy: 0.576
>>> fig = plt.figure(figsize=(10, 10))
	 
>>> for i in range(len(sample_images)):
    truth = sample_labels[i]
    prediction = predicted[i]
    plt.subplot(5, 2,1+i)
    plt.axis('off')
    color='green' if truth == prediction else 'red'
    plt.text(40, 10, "Truth:        {0}\nPrediction: {1}".format(truth, prediction), 
             fontsize=12, color=color)
    plt.imshow(sample_images[i], cmap="gray")

	 
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B1DF3160>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        0\nPrediction: 2')
<matplotlib.image.AxesImage object at 0x00000137B1A1C5C0>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B1A1C8D0>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        0\nPrediction: 2')
<matplotlib.image.AxesImage object at 0x00000137B1C83198>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B1C79EF0>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        1\nPrediction: 2')
<matplotlib.image.AxesImage object at 0x00000137B3278D30>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B3278CC0>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        2\nPrediction: 2')
<matplotlib.image.AxesImage object at 0x00000137B32A9940>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B32A9C50>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        2\nPrediction: 2')
<matplotlib.image.AxesImage object at 0x00000137B47FA4E0>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B47FA7F0>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        2\nPrediction: 2')
<matplotlib.image.AxesImage object at 0x00000137B482C0B8>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B4821E10>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        0\nPrediction: 2')
<matplotlib.image.AxesImage object at 0x00000137B7D01C50>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B7D01F60>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        2\nPrediction: 2')
<matplotlib.image.AxesImage object at 0x00000137B7D33828>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B7D33B38>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        1\nPrediction: 2')
<matplotlib.image.AxesImage object at 0x00000137B7D64400>
<matplotlib.axes._subplots.AxesSubplot object at 0x00000137B7D64710>
(0.0, 1.0, 0.0, 1.0)
Text(40,10,'Truth:        2\nPrediction: 2')
<matplotlib.image.AxesImage object at 0x00000137BA115048>
>>> plt.show()
	 
>>> x = tf.placeholder(dtype = tf.float32, shape = [None, 64, 64])
	 
>>> y = tf.placeholder(dtype = tf.int32, shape = [None])
	 
>>> images_flat = tf.contrib.layers.flatten(x)
	 
>>> logits = tf.contrib.layers.fully_connected(images_flat, 3, tf.nn.relu)
	 
>>> loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels = y, logits = logits))
	 
>>> train_op = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)
	 
>>> correct_pred = tf.argmax(logits, 1)
	 
>>> accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
	 
>>> tf.set_random_seed(1234)
	 
>>> sess = tf.Session()
	 
>>> sess.run(tf.global_variables_initializer())
	 
>>> for i in range(201):
	print('EPOCH', i)
	_, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images64_gray, y: labels})
	if i % 10 == 0:
		print("Loss: ", loss)
	print('DONE WITH EPOCH')

Loss:  Tensor("Mean_6:0", shape=(), dtype=float32)

>>> sess.run([correct_pred], feed_dict={x: sample_images})[0]
	 
array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0], dtype=int64)
>>> logits = tf.contrib.layers.fully_connected(images_flat, 3, tf.nn.leaky_relu)
	 
>>> loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels = y, logits = logits))
	 
>>> train_op = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)
	 
>>> correct_pred = tf.argmax(logits, 1)
	 
>>> accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
	 
>>> tf.set_random_seed(1234)
	 
>>> sess = tf.Session()
	 
>>> sess.run(tf.global_variables_initializer())
	 
>>> for i in range(201):
	print('EPOCH', i)
	_, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images64_gray, y: labels})
	if i % 10 == 0:
		print("Loss: ", loss)
	print('DONE WITH EPOCH')

Loss:  Tensor("Mean_8:0", shape=(), dtype=float32)

>>> sess.run([correct_pred], feed_dict={x: sample_images})[0]
	 
array([1, 0, 1, 2, 2, 2, 0, 2, 1, 1], dtype=int64)
>>> test_images, test_labels = load_data(test_data_directory)
	 
>>> test_images64 = [transform.resize(image, (64, 64)) for image in test_images]
	 
>>> test_images64 = rgb2gray(np.array(test_images64))
	 
>>> predicted = sess.run([correct_pred], feed_dict={x: test_images64})[0]
	 
>>> match_count = sum([int(y == y_) for y, y_ in zip(test_labels, predicted)])
	 
>>> accuracy = match_count / len(test_labels)
	 
>>> print("Accuracy: {:.3f}".format(accuracy))
	 
Accuracy: 0.591
>>> test_images64
	 
>>> images64_gray

>>> images64_gray.len
	 
Traceback (most recent call last):
  File "<pyshell#252>", line 1, in <module>
    images64_gray.len
AttributeError: 'numpy.ndarray' object has no attribute 'len'
>>> len(images64_gray)
	 
533
>>> labels
	 
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
>>> len(labels)
	 
533
>>> 4x_images64_gray = []
	 
SyntaxError: invalid syntax
>>> four_images64_gray = []
	 
>>> four_labels = []
	 
>>> for i in range(len(labels)):
	 four_labels.append(labels[i])
	 four_images64_gray.append(images64_gray[i])
	 for j in [90, 180, 270]:
		 four_labels.append(labels[i])
		 four_images64_gray.append(transform.rotate(images64_gray[i], j))

	 
>>> len(four_images64_gray
	)
	 
2132
>>> len(four_labels)
	 
2132
>>> four_
	 
Traceback (most recent call last):
  File "<pyshell#269>", line 1, in <module>
    four_
NameError: name 'four_' is not defined
>>> four_labels
	 
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
>>> x = tf.placeholder(dtype = tf.float32, shape = [None, 64, 64])
	 
>>> y = tf.placeholder(dtype = tf.int32, shape = [None])
	 
>>> images_flat = tf.contrib.layers.flatten(x)
	 
>>> logits = tf.contrib.layers.fully_connected(images_flat, 3, tf.nn.leaky_relu)
	 
>>> loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels = y, logits = logits))
	 
>>> train_op = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)
	 
>>> correct_pred = tf.argmax(logits, 1)
	 
>>> accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
	 
>>> tf.set_random_seed(1234)
	 
>>> sess = tf.Session()
	 
>>> sess.run(tf.global_variables_initializer())
	 
>>> for i in range(201):
	print('EPOCH', i)
	_, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: four_images64_gray, y: four_labels})
	if i % 10 == 0:
		print("Loss: ", loss)
	print('DONE WITH EPOCH')

Loss:  Tensor("Mean_10:0", shape=(), dtype=float32)

>>> sess.run([correct_pred], feed_dict={x: sample_images})[0]
	 
array([1, 0, 1, 2, 0, 2, 0, 1, 1, 1], dtype=int64)
>>> sess.run([correct_pred], feed_dict={x: test_images64})[0]
	 
array([2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0,
       0, 2, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0,
       0, 1, 1, 0, 0, 0, 1, 1, 1, 2, 2, 0, 2, 1, 1, 1, 1, 1, 1, 1, 0, 1,
       1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 0, 2, 2, 2, 2, 2, 2, 2, 0,
       1, 2, 2, 2, 1, 2, 2, 2, 2, 2, 1, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
       2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
      dtype=int64)
>>> match_count = sum([int(y == y_) for y, y_ in zip(test_labels, predicted)])
	 
>>> accuracy = match_count / len(test_labels)
	 
>>> print("Accuracy: {:.3f}".format(accuracy))
	 
Accuracy: 0.591
>>> 