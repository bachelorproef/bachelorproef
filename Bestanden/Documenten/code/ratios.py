>>> sample_indexes = random.sample(range(len(images28_gray)), 10)
>>> test_images28 = [images28_gray[i] for i in sample_indexes]
>>> sample_labels = [labels[i] for i in sample_indexes]
>>> predicted = sess.run([correct_pred], feed_dict={x: test_images28})[0]
>>> print(sample_labels)
>>> print(predicted)
>>> fig = plt.figure(figsize=(10, 10))
>>> for i in range(len(test_images28)):
    truth = sample_labels[i]
    prediction = predicted[i]
    plt.subplot(5, 2,1+i)
    plt.axis('off')
    color='green' if truth == prediction else 'red'
    plt.text(40, 10, "Truth:        {0}\nPrediction: {1}".format(truth, prediction), 
             fontsize=12, color=color)
    plt.imshow(test_images28[i], cmap="gray")

>>> plt.show()

128x128
224x224
300x300

>>> ROOT_PATH = "D:\Google Drive\\2017-2018 (3pba)\Bachelorproef - Embedded Vision - Jorim Tielemans\LEGO DUPLO\\nieuw\geknipt\\2302"

>>> file_names = []
>>> file_names = [os.path.join(ROOT_PATH, f) 
                      for f in os.listdir(ROOT_PATH) 
                      if f.endswith(".jpg")]

>>> images = []
>>> for f in file_names:
            images.append(skimage.data.imread(f))

>>> images_array = np.array(images)

>>> image_ratio = []
>>> for i in range(images_array.size):
	image_ratio.append(images_array[i].shape[1]/images_array[i].shape[0])
	
>>> image_ratio

# Show the shape and dimensions
>>> for i in range(len(traffic_signs)):
	print("shape: {0}, min: {1}, max: {2}".format(images28[traffic_signs[i]].shape, 
												  images28[traffic_signs[i]].min(), 
												  images28[traffic_signs[i]].max()))

>>> test_images, test_labels = load_data(test_data_directory)
test_images28 = []
for image in test_images:
>>> test_images28.append(transform.resize(image, (28, 28)))
>>> test_images28 = rgb2gray(np.array(test_images28))
new_image.append(rgb2gray(transform.resize(images[1], (28, 28))))
for i in [90, 180, 270]:
	images.append(transform.rotate(images[0], i))
	
images_1.append(transform.resize(images[i], (28, 28)))
images_2.append(rgb2gray(images_1[j]))
images_3.append(images_2[k])
for l in [90, 180, 270]:
	images_3.append(transform.rotate(images_2[k], l))


source_files = "D:\Google Drive\\2017-2018 (3pba)\Bachelorproef - Embedded Vision - Jorim Tielemans\LEGO DUPLO\\nieuw\geknipt\\2302"
destination_folder = "D:\Google Drive\\2017-2018 (3pba)\Bachelorproef - Embedded Vision - Jorim Tielemans\LEGO DUPLO\\nieuw\geknipt\\2302\\test"

for i in range(5):
	filename = random.choice(os.listdir(source_files))
	source_path = os.path.join(source_files, filename)
	destination_path = os.path.join(destination_folder, filename)
	os.rename(source_path, destination_path)
	