Python 3.6.4 (v3.6.4:d48eceb, Dec 19 2017, 06:54:40) [MSC v.1900 64 bit (AMD64)] on win32
Type "copyright", "credits" or "license()" for more information.
>>> import tensorflow as tf

>>> tf.__version__
'1.8.0'

# Show used device
>>> sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))

# Run on CPU
>>> with tf.device('/cpu:0'):
	# Initialize two constants
    a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
    b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
	# Multiply
    c = tf.matmul(a, b)

# Initialize Session and run `result`
>>> with tf.Session() as sess:
    print (sess.run(c))

# Run on GPU
>>> with tf.device('/gpu:0'):
    a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
    b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
    c = tf.matmul(a, b)
    
>>> with tf.Session() as sess:
    print (sess.run(c))

# Print found devices
>>> from tensorflow.python.client import device_lib
>>> print(device_lib.list_local_devices())

# Import libraries
>>> import os
>>> import random
>>> import skimage
>>> from skimage import data
# Import the `transform` module from `skimage`
>>> from skimage import transform
# Import `rgb2gray` from `skimage.color`
>>> from skimage.color import rgb2gray
# Import the `pyplot` module of `matplotlib`
>>> import matplotlib.pyplot as plt
>>> import numpy as np

# Function to load images and their respecting labels
>>> def load_data(data_directory):
    directories = [d for d in os.listdir(data_directory)
                   if os.path.isdir(os.path.join(data_directory, d))]
    labels = []
    images = []
    for d in directories:
        label_directory = os.path.join(data_directory, d)
        file_names = [os.path.join(label_directory, f) 
                      for f in os.listdir(label_directory) 
                      if f.endswith(".jpg")]
        for f in file_names:
            images.append(skimage.data.imread(f))
            labels.append(int(d))
    return images, labels

# Show a plot with some RGB images
>>> def show_some_images(images, filter):
    for i in range(len(filter)):
        plt.subplots_adjust(wspace=0.5)
        plt.subplot(1, 4, i+1)
        plt.axis('off')
        plt.imshow(images[filter[i]])
    plt.show()

# Show a plot with some gray images
>>> def show_some_gray_images(gray_images, filter):
    for i in range(len(filter)):
        plt.subplots_adjust(wspace=0.5)
        plt.subplot(1, 4, i+1)
        plt.axis('off')
        plt.imshow(gray_images[filter[i]], cmap="gray")
    plt.show()

>>> ROOT_PATH = "D:\Bachelorproef\Processed"
>>> train_data_directory = os.path.join(ROOT_PATH, "Training")
>>> test_data_directory = os.path.join(ROOT_PATH, "Testing")

# Lees de afbeeldingen en labels in
>>> images, labels = load_data(train_data_directory)

>>> images_array = np.array(images)
# Print the `images` dimensions
>>> print(images_array.ndim)
1
# Print the number of `images`'s elements
>>> print(images_array.size)
533
# Print the first instance of `images`
>>> images_array[0]

>>> labels_array = np.array(labels)
# Print the `labels` dimensions
>>> print(labels_array.ndim)
1
# Print the number of `labels`'s elements
>>> print(labels_array.size)
533
# Count the number of labels
>>> print(len(set(labels_array)))
3

# Make a histogram with 62 bins of the `labels` data
>>> plt.hist(labels, 3)
# Show the plot
>>> plt.show()

# Determine the (random) indexes of the images that you want to see 
>>> traffic_signs = [22, 170, 232, 271]

# Show a plot with the random images that you defined
>>> show_some_images(images, traffic_signs)

# Get the unique labels 
>>> unique_labels = set(labels)
>>> unique_labels
{0, 1, 2}
# Initialize the figure
>>> plt.figure(figsize=(15, 15))
<Figure size 1500x1500 with 0 Axes>
# Set a counter
>>> i = 1
# For each unique label,
>>> for label in unique_labels:
    # You pick the first image for each label
    image = images[labels.index(label)]
    # Define 64 subplots 
    plt.subplot(8, 8, i)
    # Don't include axes
    plt.axis('off')
    # Add a title to each subplot 
    plt.title("Label {0} ({1})".format(label, labels.count(label)))
    # Add 1 to the counter
    i += 1
    # And you plot this first image 
    plt.imshow(image)
# Show the plot
>>> plt.show()

# Rescale the images in the `images` array
>>> images28 = [transform.resize(image, (28, 28)) for image in images]
>>> images28_array = np.array(images28)

# SHow the shape of the array
>>> print(images28_array.shape)
(533, 28, 28, 3)

# Show a plot with the resized images
show_some_images(images28, traffic_signs)

# Make the images gray
>>> images28_gray = rgb2gray(images28_array)

# Show the shape of the array, notice only 1 color channel
>>> print(images28_gray.shape)
(533, 28, 28)

# Show a plot with the gray images
>>> show_some_gray_images(images28_gray, traffic_signs)

# !hoeveel layers zijn er!

# Initialize placeholders
>>> x = tf.placeholder(dtype = tf.float32, shape = [None, 28, 28])
>>> y = tf.placeholder(dtype = tf.int32, shape = [None])
# Flatten the input data
>>> images_flat = tf.contrib.layers.flatten(x)
>>> print("images_flat: ", images_flat)
images_flat:  Tensor("Flatten/flatten/Reshape:0", shape=(?, 784), dtype=float32)
# Het aantal labels
>>> print(len(set(labels_array)))
3
# Fully connected layer !rekening houden met aantal labels! dit variant
>>> logits = tf.contrib.layers.fully_connected(images_flat, 3, tf.nn.relu)
>>> print("logits: ", logits)
logits:  Tensor("fully_connected/Relu:0", shape=(?, 3), dtype=float32)
# Define a loss function !andere loss function testen!
>>> loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels = y, logits = logits))
>>> print("loss: ", loss)
loss:  Tensor("Mean:0", shape=(), dtype=float32)
# Define an optimizer
>>> train_op = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)
# Convert logits to label indexes
>>> correct_pred = tf.argmax(logits, 1)
>>> print("predicted_labels: ", correct_pred)
predicted_labels:  Tensor("ArgMax:0", shape=(?,), dtype=int64)
# Define an accuracy metric
>>> accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))


>>> tf.set_random_seed(1234)
>>> sess = tf.Session()
>>> sess.run(tf.global_variables_initializer())

>>> for i in range(201):
	print('EPOCH', i)
	_, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images28_gray, y: labels})
	if i % 10 == 0:
		print("Loss: ", loss)
	print('DONE WITH EPOCH')
EPOCH 0
Loss:  Tensor("Mean_4:0", shape=(), dtype=float32)
DONE WITH EPOCH

# Pick 10 random images
>>> sample_indexes = random.sample(range(len(images28_gray)), 10)
>>> sample_images = [images28_gray[i] for i in sample_indexes]
>>> sample_labels = [labels[i] for i in sample_indexes]
# Run the "correct_pred" operation
>>> predicted = sess.run([correct_pred], feed_dict={x: sample_images})[0]
# Print the real and predicted labels
>>> print(sample_labels)
[1, 0, 1, 2, 0, 2, 1, 1, 1, 2]
>>> print(predicted)
[1 0 1 2 0 2 1 1 1 2]
# Display the predictions and the ground truth visually.
>>> fig = plt.figure(figsize=(10, 10))
>>> for i in range(len(sample_images)):
    truth = sample_labels[i]
    prediction = predicted[i]
    plt.subplot(5, 2,1+i)
    plt.axis('off')
    color='green' if truth == prediction else 'red'
    plt.text(40, 10, "Truth:        {0}\nPrediction: {1}".format(truth, prediction), 
             fontsize=12, color=color)
    plt.imshow(sample_images[i], cmap="gray")
>>> plt.show()

# Load the test data
>>> test_images, test_labels = load_data(test_data_directory)
# Transform the images to 28 by 28 pixels
>>> test_images28 = [transform.resize(image, (28, 28)) for image in test_images]
# Convert to grayscale
>>> test_images28 = rgb2gray(np.array(test_images28))
# Run predictions against the full test set.
>>> predicted = sess.run([correct_pred], feed_dict={x: test_images28})[0]
# Calculate correct matches 
>>> match_count = sum([int(y == y_) for y, y_ in zip(test_labels, predicted)])
# Calculate the accuracy
>>> accuracy = match_count / len(test_labels)
# Print the accuracy
>>> print("Accuracy: {:.3f}".format(accuracy))
Accuracy: 0.667


# Test a single image
>>> image = skimage.data.imread('D:\\Bachelorproef\\Processed\\Testing\\1\\WIN_20180601_10_21_59_Pro.ppm')
>>> label = 1
>>> image = skimage.data.imread('D:\\Bachelorproef\\Processed\\Testing\\2\\WIN_20180601_10_19_46_Pro.ppm')
>>> image = skimage.data.imread('D:\\Bachelorproef\\Processed\\Testing\\2\\WIN_20180601_10_20_02_Pro.ppm')
>>> image = skimage.data.imread('D:\\Bachelorproef\\Processed\\Testing\\0\\WIN_20180601_10_25_46_Pro.ppm')
>>> image = skimage.data.imread('D:\\Bachelorproef\\Processed\\Testing\\0\\WIN_20180601_10_26_00_Pro.ppm')

>>> image = transform.resize(image, (28, 28))
>>> image = rgb2gray(image)

#
>>> image = rgb2gray(np.array(image))
>>> from numpy import array
>>> print(image.shape)
(28, 28)
#

>>> image = array(image).reshape(1, 28,28)
>>> predicted = sess.run([correct_pred], feed_dict={x: image})[0]
>>> predicted[0]
1
>>> label
1


>>> sess.close()