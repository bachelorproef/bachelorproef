Python 3.6.4 (v3.6.4:d48eceb, Dec 19 2017, 06:54:40) [MSC v.1900 64 bit (AMD64)] on win32
Type "copyright", "credits" or "license()" for more information.
>>> import requests
>>> r = requests.get('https://api.dialogflow.com/v1/')
>>> url = 'https://api.dialogflow.com/v1/query?v=20150910'
>>> headers = {'Authorization': 'Bearer 53a52106d488429f9f178ccbbc9c1227', 'Content-Type': 'application/json'}
>>> data = {'lang': 'nl', 'query': 'Hoe gaat het?', 'sessionId': '12345'}
>>> r = requests.post(url, headers=headers, json=data)
>>> r.status_code
200
>>> r.headers
{'Server': 'nginx/1.13.6', 'Date': 'Tue, 27 Mar 2018 12:04:47 GMT', 'Content-Type': 'application/json;charset=UTF-8', 'Content-Length': '718', 'X-Cloud-Trace-Context': 'c0997390b2a5f7239c159850694f53b7/1861870458869467103;o=0', 'Access-Control-Allow-Credentials': 'true', 'Via': '1.1 google', 'Alt-Svc': 'clear'}
>>> r.json()
{'id': '3002c2f3-c19b-4962-a75c-fc5508b7a823', 'timestamp': '2018-03-27T12:04:47.569Z', 'lang': 'nl', 'result': {'source': 'agent', 'resolvedQuery': 'Hoe gaat het?', 'action': 'smalltalk.greetings.whatsup', 'actionIncomplete': False, 'parameters': {}, 'contexts': [], 'metadata': {'intentId': '2e20d6e7-0c3b-44b5-a7fb-5fd92bef1b0d', 'webhookUsed': 'false', 'webhookForSlotFillingUsed': 'false', 'intentName': 'smalltalk.greetings.whatsup'}, 'fulfillment': {'speech': '', 'messages': []}, 'score': 0.75}, 'status': {'code': 200, 'errorType': 'success', 'webhookTimedOut': False}, 'sessionId': '12345'}
>>> r.json()['result']['action']