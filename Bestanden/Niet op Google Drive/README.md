Volgende bestanden zijn niet geüpload wegens hun grootte.

- lego-image-dataset.txt: https://github.com/ernestbofill/lego-image-dataset (2.79 GB)
- Pynq-Z1 v2.1 image.txt: https://files.digilent.com/Products/PYNQ/pynq_z1_v2.1.img.zip (4.60 GB)