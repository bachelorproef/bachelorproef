#Overwrite the default wpa_supplicant setup with a custom one baked in:
cp -rf ./wpa_supplicant.conf /etc

#Run it:
wpa_supplicant -Dnl80211 -iwlan0 -c/etc/wpa_supplicant.conf -B

#To add DHCP:
udhcpc -i wlan0


