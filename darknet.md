# darknet
https://github.com/AlexeyAB/darknet
git clone https://github.com/AlexeyAB/darknet.git


## Anchors
To improve object detection (and the speed of training), it's recommended to recalculate the anchors for your dataset:

darknet.exe detector calc_anchors data/obj.data -num_of_clusters 9 -width 416 -height 416

**This is specific to the width and height choosen in the cfg-file.*

If it's calculated, set the same 9 anchors in each of 3 [yolo]-layers in your cfg-file.
Remark: tiny models only use 6 anchors and have just 2 [yolo]-layers, adjust this in the command above.

Example output:
```
num_of_clusters = 9, width = 416, height = 416
 read labels from 663 images
 loaded          image: 663      box: 663
 all loaded.

 calculating k-means++ ...

 avg IoU = 87.74 %

Saving anchors to the file: anchors.txt
```

Append ` -show` to visualize these values.

## Pre-trained models

Here you can find different cfg-files for different models (smaller -> faster & lower quality). Some remarks:

- Because the GPU in my laptop (NVIDIA GeForce GTX 950M) has limited memory (4GB), we need to alter the subdivisions.
- We need to set `classes:` to the number of objects we have: 3.
- Last we need to change the value of `filter:`. For Yolov3 this can be calculated with (classes + 5)*3.
  Full formula is actually (classes + coords + 1)*masks, with mask the indices of anchors (mask=..., here always 3).

### yolov3

Based on **yolov3.cfg** - config for Yolo v3, requires 4 GB GPU-RAM (236 MB)
**darknet53.conv.74** - weights for the convolutional layers (154 MB)

- `batch:` of `[net]` = 64
- `subdivisions:` of `[net]` = 8 -> 16 -> 32!
- `classes:` *in each of the 3 `[yolo]`-layers* = 3
- `filters:` *in the `[convolutional]`-layers before each [yolo] layer* = 24

anchors = 84.6206,117.3424, 97.9351,139.4392, 133.8207,112.7230, 86.1343,204.3517, 116.1660,165.0755, 162.7918,149.7626, 119.4197,209.5442, 156.0657,195.3135, 155.6441,243.9599
originally: anchors = 10,13,  16,30,  33,23,  30,61,  62,45,  59,119,  116,90,  156,198,  373,326

To train, test and use the models, use the commands below with:

- yolo-obj.cfg = yolov3.cfg
- yolo-obj.conv = darknet53.conv.74
- yolo-obj_1000.weight = yolov3_1000.weights (with 400, 800, 1000 and 1200; file in backup/)

### yolov3-tiny

Based on **yolov3-tiny.cfg** - config for Yolo v3 tiny, requires 1 GB GPU-RAM (34 MB)
**yolov3-tiny.conv.15** - the weights for the convolutional layers is not included, follow the steps below to get it.

Download default weights-file for yolov3-tiny: pjreddie.com/media/files/yolov3-tiny.weights
Get pre-trained weights for the convolutional layers yolov3-tiny.conv.15 by using this command:
   darknet.exe partial yolov3-tiny_conv-weights.cfg yolov3-tiny.weights yolov3-tiny.conv.15 15
This has already been done for you, all files can be found in the 'pre-trained weights-file'-folder.

- batch of `[net]` = 64
- subdivisions of `[net]` = 2 -> 4 -> 8!
- `classes:` *in each of the 2 `[yolo]`-layers* = 3
- `filters:` *in the `[convolutional]`-layers before each [yolo] layer* = 24

anchors = 87.9634,116.8262, 99.0232,140.2528, 117.2295,167.7278, 101.9841,208.5934, 157.8845,138.4382, 153.7807,212.9310
originally: anchors = 10,14,  23,27,  37,58,  81,82,  135,169,  344,319

To train, test and use the models, use the commands below with:

- yolo-obj.cfg = yolov3-tiny.cfg
- yolo-obj.conv = yolov3-tiny.conv.15
- yolo-obj_1000.weight = yolov3-tiny_1000.weights (with 500, 1000, 1500 and 2000; file in backup/)

### yolov3-tiny_xnor

Based on **yolov3-tiny_xnor.cfg**; the weights-file used here,
is the same as in previous model: **yolov3-tiny.conv.15**

This model has an added `xnor=1` parameter for each [convolutional]-layer,
except the 1st-layer (line 25) and layers with activation=linear
(which are before each [yolo] or [region] layers, line 131 and 177).

- batch of `[net]` = 64
- subdivisions of `[net]` = 2 -> 4 -> 8!
- `classes:` *in each of the 2 `[yolo]`-layers* = 3
- `filters:` *in the `[convolutional]`-layers before each [yolo] layer* = 24

anchors = 87.9634,116.8262, 99.0232,140.2528, 117.2295,167.7278, 101.9841,208.5934, 157.8845,138.4382, 153.7807,212.9310
originally: anchors = 10,14,  23,27,  37,58,  81,82,  135,169,  344,319

To train, test and use the models, use the commands below with:

- yolo-obj.cfg = yolov3-tiny_xnor.cfg
- yolo-obj.conv = yolov3-tiny.conv.15
- yolo-obj_1000.weight = yolov3-tiny_xnor_1000.weights (with 500, 1000, 1500 and 2000)

Detection of XNOR-net is accelerated 4x times (20-25 FPS) on CPU if your CPU supports AVX2.
For detection, maybe a lower threshold than usual should be used.
For example, try to use flag `-thresh 0.1`.

## darknet commands

To train, test and use the models, the following commands are used:
Remark: when deploying the models, make sure to use the best performing .weights-file.

### Start training

To monitor GPU, use `C:\Program Files\NVIDIA Corporation\NVSMI>nvidia-smi.exe` in cmd.

   darknet.exe detector train data/obj.data yolo-obj.cfg yolo-obj.conv

To store the log output in a file, append ` >log.txt`.

### Continue training

When stopping or on crash, you can use the partially-trained model in backup/ to run training again:

   darknet.exe detector train data/obj.data yolo-obj.cfg backup/yolo-obj_600.weights

Make sure not to forget logging again.

### Test the model

Usually it's sufficient to train 2000 iterations for each class (object).
For a more precise definition when you should stop training, use the following manual:

1. During training, you will see varying indicators of error:

    ```
    1000: 0.533078, 0.587592 avg loss, 0.001000 rate, 56.033875 seconds, 64000 images
    1100: 0.382463, 0.401773 avg loss, 0.001000 rate, 31.852291 seconds, 70400 images
    1200: 0.299099, 0.331089 avg loss, 0.001000 rate, 31.686680 seconds, 76800 images
    ```

    - 1000 - iteration number (number of batch)
    - 0.587592 avg loss - average loss (error) - the lower, the better

    When you see that average loss (0.xxxxxx) no longer decreases at many iterations, then you should stop training.

2. Once training is stopped, you should take some of last .weights-files from backup/ and choose the best of them:

    For example, you stopped training after 1200 iterations, but one of previous weights (1000, 1100, 1200) can give the best results.
    This can happen due to **Overfitting** - when you can detect objects on images from the training-dataset,
    but can't detect objects on any others images. To avoid this you should get weights from an **Early Stopping Point**:

    1. At first, in your file obj.data you must specify the path to the validation dataset valid = valid.txt (format of valid.txt as in train.txt).
        If you don't have validation images, just copy data\train.txt to data\valid.txt (or use train.txt).

    2. If training is stopped after 1200 iterations, validate some of the previous weights using these commands:

        ```
        darknet.exe detector map data/obj.data yolo-obj.cfg backup/yolo-obj_1000.weights
        darknet.exe detector map data/obj.data yolo-obj.cfg backup/yolo-obj_1100.weights
        darknet.exe detector map data/obj.data yolo-obj.cfg backup/yolo-obj_1200.weights
        ```

        And compare the last output lines for each weights (1000, 1100, 1200):

    3. Choose the weights-file with the highest IoU (intersect of union) and mAP (mean average precision)

        For example, if yolo-obj_1100.weights gives bigger IoU values - then use this weights for detection.

        Example of custom object detection: darknet.exe detector test data/obj.data yolo-obj.cfg yolo-obj_8000.weights

        - IoU (intersect of union) - average instersect of union of objects and detections for a certain threshold = 0.25
        - mAP (mean average precision) - mean value of average precisions for each class,
            where average precision is average value of 11 points on PR-curve for each possible threshold (each probability of detection)
            for the same class (Precision-Recall in terms of PascalVOC, where Precision=TP/(TP+FP) and Recall=TP/(TP+FN) ).

            mAP is default metric of precision in the PascalVOC competition, this is the same as AP50 metric in the MS COCO competition.
            In terms of Wiki, indicators Precision and Recall have a slightly different meaning than in the PascalVOC competition, but IoU always has the same meaning.

3. Append ` >log.txt` to store the testing output in a file (same as with training).

### Use the model

In this case we use `demo` for a video, use `test` for still images.

   darknet.exe detector demo data/obj.data yolo-obj.cfg backup/yolo-obj_1100.weights -c 0 (or 1)

Add ` -http_port 8090 -dont_show` to create a mjpeg stream on http://localhost:8090; ` -out_filename res.avi` will store the output in a video file.
