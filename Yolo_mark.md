# Yolo_mark
https://github.com/AlexeyAB/Yolo_mark
git clone https://github.com/AlexeyAB/Yolo_mark.git


## list empty labels
```powershell
dir -recurse | where {$_.extension -eq ".txt" -and $_.length -ne 39} | sort length | ft fullname, length -auto
```

## list all images in a folder, then trim the spaces and prepend with path
```powershell
dir -recurse | where {$_.extension -eq ".jpg"} | ft Name -HideTableHeaders | Out-File train.txt
(get-content "train.txt") | foreach {"data/img/" + $_.Trim() } | Out-File train.txt
```

## fix line endings
Uses https://waterlan.home.xs4all.nl/dos2unix.html, make sure to add to system 'path' variable
```shell_session
for /R %G in (*.txt) do unix2dos "%G"
```

## list full path to images
Can be used when evaluating still images with darknet
```powershell
dir -recurse | where {$_.extension -eq ".jpg"} | ft fullname
```

## Some codes I used if there were multiple labels for an image

### Common
```python
float_1 = [None] * 5
float_2 = [None] * 5
new_float = [None] * 5
new_string = [None] * 5
import os
```

### Merge files that were labelled twice to get average
```
dir_old = "D:\\Documents\\Git\\bachelorproef\\jorim\\2302-gelijk"
dir_new = "D:\\Documents\\Git\\bachelorproef\\jorim\\2302\\img"

for file in os.listdir(dir_old):
    if file.endswith(".txt"):
        breaker = False
        path_file_old = os.path.join(dir_old, file)
        file_old = open(path_file_old, "r")
        string_1 = file_old.readline().split()
        file_old.close()
        path_file_new = os.path.join(dir_new, file)
        file_new = open(path_file_new, "r")
        string_2 = file_new.readline().split()
        file_new.close()
        float_1[0] = int(string_1[0])
        float_2[0] = int(string_2[0])
        for i in range(1,5):
            float_1[i] = float(string_1[i])
            float_2[i] = float(string_2[i])
        if float_1[0] != float_2[0]:
            print("class error in " + file)
            continue
        new_float[0] = float_1[0]
        new_string[0] = str(new_float[0])
        for i in range(1,5):
            if abs(float_2[i] - float_1[i]) > 0.02:
                breaker = True
            new_float[i] = (float_1[i] + float_2[i])/2
            new_string[i] = '%.6f' % new_float[i]
        if(breaker):
            print("deviation error in " + file)
            continue
        string_all = " ".join(new_string)
        os.remove(path_file_old)
        file_new = open(path_file_new, 'w')
        file_new.write(string_all + '\n')
        file_new.close()
        print("New string for " + file + ": " + string_all)
```

### Merge files with duplicate image and remove one
```python
dir_dubbel = "D:\\Documents\\Git\\bachelorproef\\jorim\\2302\\img"
path_file_dubbel = "D:\\Documents\\Git\\bachelorproef\\jorim\\2302\\img\\dubbel.txt"

file_dubbel = open(path_file_dubbel, "r")
lines = file_dubbel.read().splitlines()
file_dubbel.close()
for j in range (0, len(lines), 2):
    breaker = False
    path_file_old = os.path.join(dir_dubbel, lines[j]+".txt")
    file_old = open(path_file_old, "r")
    string_1 = file_old.readline().split()
    file_old.close()
    path_file_new = os.path.join(dir_dubbel, lines[j+1]+".txt")
    file_new = open(path_file_new, "r")
    string_2 = file_new.readline().split()
    file_new.close()
    float_1[0] = int(string_1[0])
    float_2[0] = int(string_2[0])
    for i in range(1,5):
        float_1[i] = float(string_1[i])
        float_2[i] = float(string_2[i])
    if float_1[0] != float_2[0]:
        print("class error in " + lines[j])
        continue
    new_float[0] = float_1[0]
    new_string[0] = str(new_float[0])
    for i in range(1,5):
        if abs(float_2[i] - float_1[i]) > 0.02:
            breaker = True
        new_float[i] = (float_1[i] + float_2[i])/2
        new_string[i] = '%.6f' % new_float[i]
    if(breaker):
        print("deviation error in " + lines[j])
        continue
    string_all = " ".join(new_string)
    img_file_new = os.path.join(dir_dubbel, lines[j+1]+".jpg")
    os.remove(img_file_new)
    os.remove(path_file_new)
    file_old = open(path_file_old, 'w')
    file_old.write(string_all + '\n')
    file_old.close()
    print("New string for " + lines[j] + ": " + string_all)
```