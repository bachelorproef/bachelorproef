# Script to convert yolo annotations to voc format

# Based on https://gist.github.com/goodhamgupta/7ca514458d24af980669b8b1c8bcdafd
# with modifications from https://gist.github.com/yli3/683a3b93b5ec5735114d9800745acbed

# Sample format
# <annotation>
#     <folder>_image_fashion</folder>
#     <filename>brooke-cagle-39574.jpg</filename>
#     <size>
#         <width>1200</width>
#         <height>800</height>
#         <depth>3</depth>
#     </size>
#     <segmented>0</segmented>
#     <object>
#         <name>head</name>
#         <pose>Unspecified</pose>
#         <truncated>0</truncated>
#         <difficult>0</difficult>
#         <bndbox>
#             <xmin>549</xmin>
#             <ymin>251</ymin>
#             <xmax>625</xmax>
#             <ymax>335</ymax>
#         </bndbox>
#     </object>
# <annotation>
"""
Usage:
  python yolo_to_voc.py
To beautify the files:
  for /R %x in (*.xml) do xmllint --format "%x" --output "%x"
"""
import os
import xml.etree.cElementTree as ET
from PIL import Image

ANNOTATIONS_DIR_PREFIX = "C:/bachelorproef/labeled images/pascal voc/labels"
IMAGES_DIR_PREFIX = "C:/bachelorproef/labeled images/pascal voc/images"
DESTINATION_DIR = "C:/bachelorproef/labeled images/pascal voc/annotations"

CLASS_MAPPING = {
    # Add your remaining classes here.
    '0': '2302',
    '1': '3011',
    '2': '3437'
}


def create_root(file_prefix, image_file_path, width, height):
    root = ET.Element("annotations")
    ET.SubElement(root, "folder").text = "images"
    ET.SubElement(root, "filename").text = "{}.jpg".format(file_prefix)
    ET.SubElement(root, "path").text = image_file_path
    source = ET.SubElement(root, "source")
    ET.SubElement(source, "database").text = "Unknown"
    size = ET.SubElement(root, "size")
    ET.SubElement(size, "width").text = str(width)
    ET.SubElement(size, "height").text = str(height)
    ET.SubElement(size, "depth").text = "3"
    ET.SubElement(root, "segmented").text = "0"
    return root


def create_object_annotation(root, voc_labels):
    for voc_label in voc_labels:
        obj = ET.SubElement(root, "object")
        ET.SubElement(obj, "name").text = voc_label[0]
        ET.SubElement(obj, "pose").text = "Unspecified"
        ET.SubElement(obj, "truncated").text = str(0)
        ET.SubElement(obj, "difficult").text = str(0)
        ET.SubElement(obj, "occluded").text = str(0)
        bbox = ET.SubElement(obj, "bndbox")
        ET.SubElement(bbox, "xmin").text = str(round(voc_label[1]))
        ET.SubElement(bbox, "ymin").text = str(round(voc_label[2]))
        ET.SubElement(bbox, "xmax").text = str(round(voc_label[3]))
        ET.SubElement(bbox, "ymax").text = str(round(voc_label[4]))
    return root


def create_file(file_prefix, image_file_path, width, height, voc_labels):
    root = create_root(file_prefix, image_file_path, width, height)
    root = create_object_annotation(root, voc_labels)
    tree = ET.ElementTree(root)
    tree.write("{}/{}.xml".format(DESTINATION_DIR, file_prefix))


def read_file(file_path):
    file_prefix = file_path.split(".txt")[0]
    annotations_file_path = "{}/{}".format(ANNOTATIONS_DIR_PREFIX, file_path)
    image_file_path = "{}/{}.jpg".format(IMAGES_DIR_PREFIX, file_prefix)
    img = Image.open(image_file_path)
    w, h = img.size
    with open(annotations_file_path, 'r') as file:
        lines = file.readlines()
        voc_labels = []
        for line in lines:
            voc = []
            line = line.strip()
            data = line.split()
            voc.append(CLASS_MAPPING.get(data[0]))
            center_x = float(data[1]) * w
            center_y = float(data[2]) * h
            bbox_width = float(data[3]) * w
            bbox_height = float(data[4]) * h
            voc.append(center_x - (bbox_width / 2))
            voc.append(center_y - (bbox_height / 2))
            voc.append(center_x + (bbox_width / 2))
            voc.append(center_y + (bbox_height / 2))
            voc_labels.append(voc)
        create_file(file_prefix, image_file_path, w, h, voc_labels)
    print("Processing complete for file: {}".format(annotations_file_path))


def start():
    if not os.path.exists(DESTINATION_DIR):
        os.makedirs(DESTINATION_DIR)
    for filename in os.listdir(ANNOTATIONS_DIR_PREFIX):
        if filename.endswith('txt'):
            read_file(filename)
        else:
            print("Skipping file: {}".format(filename))


if __name__ == "__main__":
    start()
